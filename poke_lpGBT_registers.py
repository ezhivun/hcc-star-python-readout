/*

Update lpGBT registers one by one over the optical link
and read back the values. Helpful for troubleshooting
the situation when lpGBT loses connection with FELIX
mid-configuration.

*/

#!/usr/bin/env python3

import subprocess, pdb, re, time

p = re.compile('\\s+([a-fA-F0-9]+):\\s+([a-fA-F0-9]+)')

with open("lpgbt_stave_ppa.txt", 'r') as f:
	regs = f.read().split('\n')


for i in range(len(regs)):
#for i in range(2):	
	reg = regs[i]
	print(f"Writing 0x{reg} to register 0x{i:02x}")
	cmd_wr = ['fice', '-G', '0', '-I', '71', '-a', f'0x{i:02x}', f'0x{reg}']
	print(" ".join(cmd_wr))
	result_wr = subprocess.run(cmd_wr, stdout=subprocess.PIPE)		
	#time.sleep(0.2)
	cmd_rd = ['fice', '-G', '0', '-I', '71', '-a', f'0x{i:02x}']
	print(" ".join(cmd_rd))
	result_rd = subprocess.run(cmd_rd, stdout=subprocess.PIPE)

	result = result_rd.stdout.decode('utf-8')

	match = p.findall(result)
	if match:
		rreg = int(match[0][0])
		rval = int(match[0][1], 16)
		print(f"Read back 0x{rval} from register {rreg:02x}")
		if rreg != i:
			print("Received response for a different register")
			print(f"Expected 0x{i:02x}, was 0x{rreg:02x}")
			print(match)

		if rval != int(reg, 16):
			print("Register read back value doesn't match the written value")
			print(f"Expected 0x{reg}, was 0x{rval:02x}")
			print(match)
		
	else:
	 	print("No response")
	 	break


