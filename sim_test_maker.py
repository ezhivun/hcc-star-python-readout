#!/usr/bin/env python3

from hcc_star.parser import Parser as HCCParser
from hcc_star.parser import TYP
from hcc_star.driver import get_driver
from hcc_star.parser import get_target_type
from hcc_star.driver_file_writer import HCCStarDriverFileWriter

import argparse
from time import sleep


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


def main():
  parser = argparse.ArgumentParser(description="Utility for preparing tests for Strips module simulation testbench. Will output files with commands and responses expected from the Strips modules")
  parser.add_argument('command', help=('command to run: reg_read, reg_write, fast, l0a, r3l1'), type=str)
  parser.add_argument('-bc', '--bc', help='bunch counter for fast command and reset', default=0, required=False, type=int)
  parser.add_argument('-fc', '--fast_command_id', help='ID of the fast command to send (default 0)', default="0", required=False, type=str)
  parser.add_argument('-hi', '--hcc_id', help='HCC ID (default broadcast)', default="0b1111", required=False, type=str)
  parser.add_argument('-ai', '--abc_id', help='ABC ID (default broadcast)', default="0b1111", required=False, type=str)
  parser.add_argument('-ri', '--register_id', help='Register ID (default 0)', default="0", required=False, type=str)
  parser.add_argument('-rd', '--register_data', help='Data to write to the register (default 0)', default="0", required=False, type=str)
  parser.add_argument('-t', '--target', help='Register read/write target (ABC or HCC, default HCC)', default="HCC", required=False, type=str)
  parser.add_argument('-v', '--verbose', help='Verbose output (-vv = even more verbose)', action="count", default=0)
  parser.add_argument('-bcr', '--bcr', help='Set bunch counter reset in L0A packet (no BCR by default)', action="store_true")
  parser.add_argument('-m', '--mask', help='L0A mask (defalt 0b1111)', default="0b1111", required=False, type=str)
  parser.add_argument('-tg', '--tag', help='L0A tag (defalt 0x1A)', default="0x1A", required=False, type=str)
  parser.add_argument('-drv', '--driver', help='ITk Strips driver version (software, firmware, plain_text), default software', default="software", required=False, type=str)
  parser.add_argument('-se', '--serial', help='ASIC serial for dynamic addressing (defalt 0)', default="0", required=False, type=str)
  parser.add_argument('-r', '--reset', help='Reset elinks when sending fdaq', action="store_true")
  parser.add_argument('-if', '--inputs_file', help='Where to store module inputs (default = module_inputs.txt)', default="module_inputs.txt", required=False, type=str)
  parser.add_argument('-of', '--outputs_file', help='Where to store expected module outputs (default = module_outputs.txt)', default="module_outputs.txt", required=False, type=str)

  global args
  args = parser.parse_args()

  driver_in = get_driver(args.driver, HCCStarDriverFileWriter)(
    target=args.target,
    hcc_id=args.hcc_id, abc_id=args.abc_id,
    verbose=args.verbose, bc=args.bc,
    nibble_order_to_host="direct", 
    nibble_order_from_host="direct", raise_on_error=False,
    reset=args.reset)
  driver_in.output_file_name = args.inputs_file

  driver_out = get_driver("software", HCCStarDriverFileWriter)(
    target=args.target,
    hcc_id=args.hcc_id, abc_id=args.abc_id,
    verbose=args.verbose, bc=args.bc,
    nibble_order_to_host="direct",
    nibble_order_from_host="direct", raise_on_error=False,
    reset=args.reset)
  driver_out.output_file_name = args.outputs_file

  drivers = [driver_in, driver_out]

  for driver in drivers:
    if is_register_read(args.command):
      driver.comments = "Register read ({}), hcc_id={}, abc_id={}, register_id={}".format(
        args.target, args.hcc_id, args.abc_id, args.register_id)
      read_register(driver, args.register_id)

    elif is_register_write(args.command):
      driver.comments = "Register write ({}), hcc_id={}, abc_id={}, register_id={}, data={}".format(
        args.target, args.hcc_id, args.abc_id, args.register_id, args.register_data)
      driver.write_register(args.register_id, args.register_data)

    elif args.command == 'fast':
      driver.comments = "Fast command, BC={}, command={}".format(
        args.bc, args.fast_command_id)
      driver.fast_command(args.fast_command_id)

    elif is_l0a(args.command):
      driver.comments = "L0A, BCR={}, mask={}, tag={}".format(
        args.bcr, args.mask, args.tag)
      driver.L0A(args.bcr, args.mask, args.tag)

    elif is_r3l1(args.command):
      driver.comments = "R3/L1, mask={}, tag={}".format(args.mask, args.tag)
      driver.R3L1(args.mask, args.tag)

    elif is_idle(args.command):
      driver.comments = "IDLE frame"
      driver.send_idle()

    else:
      parser.error("Unsupported command: {}".format(args.command))
    
  return 0


# get the spelling out of the way

def is_register_read(cmd):
  cmd = cmd.lower()
  return ((cmd == 'reg_read') or (cmd == 'register_read') or
    (cmd == 'read_reg') or (cmd == 'read_register') or (cmd == 'read'))

def is_register_write(cmd):
  cmd = cmd.lower()
  return ((cmd == 'reg_write') or (cmd == 'register_write') or
    (cmd == 'write_reg') or (cmd == 'write_register') or (cmd == 'write'))

def is_l0a(cmd):
  cmd = cmd.lower()
  return cmd == 'l0a'

def is_r3l1(cmd):
  cmd = cmd.lower()
  return cmd == 'r3l1'

def is_idle(cmd):
  cmd = cmd.lower()
  return cmd == 'idle'



def read_register(driver, register_id):
  driver.read_register(register_id)


def enable_abc(driver):
  driver.enable_ABC_star()
  sleep(1)
  driver.listen()
  hpr_report_helper(driver)



if __name__ == '__main__':
  main()
  # try:
  #   main()
  # except NameError as e:
  #   raise(e)
  # except TypeError as e:
  #   raise(e)
  # except Exception as e:
  #   print(e)
