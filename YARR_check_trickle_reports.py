#!/usr/bin/env python3

import argparse
import os
from os import path
import re
import json
import csv

__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


config_name = "Barrel_LS{}.json"
report_name_pattern = re.compile("trickle_stats_hcc(?P<id>\d+)\.txt")


def main():
	parser = argparse.ArgumentParser(description=("Utility for checking whether "
		+ "trickle configuration test results are consistent with the configuration"))
	parser.add_argument("-c", "--config", help="Path to configuration files for individual modules",
		required=True, type=str)
	parser.add_argument("-r", "--report", help="Path to test reports", required=True, type=str)
	parser.add_argument("-o", "--output", help="Where to save reports converted to .csv", required=False, type=str)

	global args
	args = parser.parse_args()

	report_file_names = [file for file in os.listdir(args.report)
		if path.isfile(path.join(args.report, file))]

	all_match = True
	all_report_data = {}

	for file_name in report_file_names:
		all_match &= verify_report(file_name, all_report_data)

	if all_match:
		print("No register value mismatch in any report ({} reports total)".format(len(report_file_names)))
	else:
		print("There was a mismatch between the registers between the trickle configuration and report".format(total))

	if args.output:
		export_all_csv(args.output, all_report_data)



# verify that the register values from the report match the register values from the configuration
def verify_report(file_name, all_report_data):
	# find which HCC ID it is
	match = report_name_pattern.match(file_name)
	report_id = match.group("id")

	report_file_name = path.join(args.report, file_name)
	print("Loading trickle report file {}".format(report_file_name))
	with open(report_file_name, "r") as report:
		report_data = parse_report(report)
		all_report_data[report_id] = report_data

	# print(json.dumps(report_data, indent=4))
	# return

	config_file_name = path.join(args.config, config_name.format(report_id))
	print("Loading trickle configuration file {}".format(config_file_name))
	with open(config_file_name, "r") as configuration:
		configuration_data = json.load(configuration)		

	all_match = True
	print("Verify that the HCC* registers match the expected values")
	all_match &= verify_registers(report=report_data["HCC"],
		config=configuration_data["HCC"]["regs"], register_map=hcc_register_names)

	print("Verify that the ABC* registers match the expected values")
	for abc_id in configuration_data["ABCs"]["IDs"]:
		print("Checking ABC_ID={}".format(abc_id))
		all_match &= verify_registers(report=report_data["ABCs"][abc_id],
			config=configuration_data["ABCs"]["common"], register_map=abc_register_names)		

	print()	
	return all_match

# check that all HCC* or ABC* registers match, report mismatches or missing registers
def verify_registers(report, config, register_map):
	all_match = True
	total = 0
	missing_from_report = []
	missing_from_config = []
	for name, value in config.items():
		register_id = register_map[name]
		register_value = int(value, 16)
		total += 1
		if not (register_id in report):
			missing_from_report.append(register_id)
		elif report[register_id] != register_value:
			all_match = False
			print("Mismatched register value for {}: expected 0x{:x}, actual 0x{:x}".format(
				register_id, register_value, report[register_id]))

	for register_id, register_value in report.items():
		if not register_id in config:
			missing_from_config.append(register_id)

	if missing_from_report:
		print(("These registers are found in trickle configuration, "
			+ "but not in the report: {}").format(missing_from_report))

	if missing_from_config:
		print(("These registers are found in trickle report, "
			+ "but not in the configuration: {}").format(missing_from_config))		

	if all_match:
		print(("No register value mismatch in {} registers found in both "
			+ "configuration and report").format(total))

	return all_match


# patterns for file parsing
abc_id_pattern = re.compile("ID:\s(?P<id>\d+)")
table_line_pattern = re.compile("(?P<id>\d+)\s+(?P<value>0x(?:\d|[a-fA-F])+)\s+(?P<occurences>\d+)")


# convert plain text report to a dictionary similar to configuration file
def parse_report(report_file):
	result = {"ABCs" : {}, "HCC" : {}}
	state = "idle"
	abc_id = 0

	for line in report_file:
		if line.strip() == "HCC":
			state = "reading_hcc"
		elif line.strip() == "ABCs":
			state = "reading_abc"
		elif line.strip() == "":
			pass

		# is it ABC id?
		match = abc_id_pattern.match(line)
		if match:
			abc_id = int(match.group("id"))
			result["ABCs"][abc_id] = {}

		# is it a data line in the table?
		match = table_line_pattern.match(line)
		if match:
			register_id = int(match.group("id"))
			register_value = int(match.group("value"), 0)
			if state == "reading_hcc":
				result["HCC"][register_id] = register_value
			elif state == "reading_abc":
				result["ABCs"][abc_id][register_id] = register_value
			else:
				raise RuntimeError("Encountered table line in IDLE state")

	return result


def export_all_csv(output_folder, report_data):	
	hcc_registers = set()
	hcc_data = {}	
	for hcc_id, data in report_data.items():
		hcc_registers.update(data["HCC"].keys())
		hcc_data[hcc_id] = data["HCC"]

	hcc_ids = sorted(report_data.keys())
	hcc_report = make_report(columns=hcc_ids, rows=list(sorted(hcc_registers)),
		data=hcc_data, filename=path.join(output_folder, 'HCC.csv'),
		row_names=hcc_register_names)
	
	for hcc_id, hcc_data in report_data.items():
		abc_registers = set()
		for abc_id, data in hcc_data["ABCs"].items():
			abc_registers.update(data.keys())

		abc_report = make_report(columns=sorted(hcc_data["ABCs"].keys()),
			rows=list(sorted(abc_registers)), data=hcc_data["ABCs"],
			filename=path.join(output_folder, 'HCC{}_ABCs.csv'.format(hcc_id)),
			row_names=abc_register_names)


def make_report(columns, rows, data, filename, row_names):
	report = []
	for row in rows:
		report_line = ["N/A"] * len(columns)		
		for col in columns:
			if row in data[col]:
				report_line[int(col)] = "{:08x}".format(data[col][row])				
		report.append(report_line)
		report_line.insert(0, "{}".format(
			list(row_names.keys())[list(row_names.values()).index(row)]))

	print("Writing report {}".format(filename))
	with open(filename, 'w', newline='') as csvfile:
		writer = csv.writer(csvfile, dialect="excel")
		for row in report:
			writer.writerow(row)

	columns.insert(0, "")
	report.insert(0, columns)
	return report


# dictionaries to lookup register ID given name

hcc_register_names = {"SEU1" : 0, "SEU2" : 1, "SEU3" : 2, "FrameRaw" : 3, "LCBerr" : 4,
	"ADCStatus" : 5, "Status" : 6, "HPR" : 15, "Pulse" : 16, "Addressing" : 17,
    "Delay1" : 32, "Delay2" : 33, "Delay3" : 34, "PLL1" : 35, "PLL2" : 36,
    "PLL3" : 37, "DRV1" : 38, "DRV2" : 39, "ICenable" : 40, "OPmode" : 41,
    "OPmodeC" : 42, "Cfg1" : 43, "Cfg2" : 44, "ExtRst" : 45, "ExtRstC" : 46,
    "ErrCfg" : 47, "ADCcfg" : 48}

abc_register_names = {"SCReg" : 0, "ADCS1" : 1, "ADCS2" : 2, "ADCS3" : 3, "ADCS4" : 4,
	"ADCS5" : 5, "ADCS6" : 6, "ADCS7" : 7, "MaskInput0" : 16, "MaskInput1" : 17,
	"MaskInput2" : 18, "MaskInput3" : 19, "MaskInput4" : 20, "MaskInput5" : 21,
	"MaskInput6" : 22, "MaskInput7" : 23, "CREG0" : 32, "CREG1" : 33, "CREG2" : 34,
	"CREG3" : 35, "CREG4" : 36, "CREG5" : 37, "CREG6" : 38, "STAT0" : 48,
	"STAT1" : 49, "STAT2" : 50, "STAT3" : 51, "STAT4" : 52, "HPR" : 63, 
	"TrimDAC0" : 64, "TrimDAC1" : 65, "TrimDAC2" : 66, "TrimDAC3" : 67,
	"TrimDAC4" : 68, "TrimDAC5" : 69, "TrimDAC6" : 70, "TrimDAC7" : 71,
	"TrimDAC8" : 72, "TrimDAC9" : 73, "TrimDAC10" : 74, "TrimDAC11" : 75,
	"TrimDAC12" : 76, "TrimDAC13" : 77, "TrimDAC14" : 78, "TrimDAC15" : 79,
	"TrimDAC16" : 80, "TrimDAC17" : 81, "TrimDAC18" : 82, "TrimDAC19" : 83,
	"TrimDAC20" : 84, "TrimDAC21" : 85, "TrimDAC22" : 86, "TrimDAC23" : 87,
	"TrimDAC24" : 88, "TrimDAC25" : 89, "TrimDAC26" : 90, "TrimDAC27" : 91,
	"TrimDAC28" : 92, "TrimDAC29" : 93, "TrimDAC30" : 94, "TrimDAC31" : 95,
	"TrimDAC32" : 96, "TrimDAC33" : 97, "TrimDAC34" : 98, "TrimDAC35" : 99,
	"TrimDAC36" : 100, "TrimDAC37" : 101, "TrimDAC38" : 102, "TrimDAC39" : 103,
    "CalREG0" : 104, "CalREG1" : 105, "CalREG2" : 106, "CalREG3" : 107,
    "CalREG4" : 108, "CalREG5" : 109, "CalREG6" : 110, "CalREG7" : 111,
    "HitCountREG0" : 128, "HitCountREG1" : 129, "HitCountREG2" : 130,
    "HitCountREG3" : 131, "HitCountREG4" : 132, "HitCountREG5" : 133,
    "HitCountREG6" : 134, "HitCountREG7" : 135, "HitCountREG8" : 136,
    "HitCountREG9" : 137, "HitCountREG10" : 138, "HitCountREG11" : 139,
    "HitCountREG12" : 140, "HitCountREG13" : 141, "HitCountREG14" : 142,
    "HitCountREG15" : 143, "HitCountREG16" : 144, "HitCountREG17" : 145,
    "HitCountREG18" : 146, "HitCountREG19" : 147, "HitCountREG20" : 148,
    "HitCountREG21" : 149, "HitCountREG22" : 150, "HitCountREG23" : 151,
    "HitCountREG24" : 152, "HitCountREG25" : 153, "HitCountREG26" : 154,
    "HitCountREG27" : 155, "HitCountREG28" : 156, "HitCountREG29" : 157,
    "HitCountREG30" : 158, "HitCountREG31" : 159, "HitCountREG32" : 160,
    "HitCountREG33" : 161, "HitCountREG34" : 162, "HitCountREG35" : 163,
    "HitCountREG36" : 164, "HitCountREG37" : 165, "HitCountREG38" : 166,
    "HitCountREG39" : 167, "HitCountREG40" : 168, "HitCountREG41" : 169,
    "HitCountREG42" : 170, "HitCountREG43" : 171, "HitCountREG44" : 172,
    "HitCountREG45" : 173, "HitCountREG46" : 174, "HitCountREG47" : 175,
    "HitCountREG48" : 176, "HitCountREG49" : 177, "HitCountREG50" : 178,
    "HitCountREG51" : 179, "HitCountREG52" : 180, "HitCountREG53" : 181,
    "HitCountREG54" : 182, "HitCountREG55" : 183, "HitCountREG56" : 184,
    "HitCountREG57" : 185, "HitCountREG58" : 186, "HitCountREG59" : 187,
    "HitCountREG60" : 188, "HitCountREG61" : 189, "HitCountREG62" : 190,
    "HitCountREG63" : 191}

if __name__ == '__main__':
    main()
