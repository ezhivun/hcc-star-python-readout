#!/usr/bin/env python3

from hcc_star.parser import Parser as HCCParser
from hcc_star.parser import TYP
from hcc_star.driver import get_driver
from hcc_star.parser import get_target_type

import argparse
from time import sleep


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


def main():
  parser = argparse.ArgumentParser(description="Utility for sending basic HCCStar commands")
  parser.add_argument('command', choices=(["read", "reg_read", "write", "reg_write", "reset", "fast", "listen", 
    "enable_abc", "l0a", "set_hcc_id", "disable_hpr", "configure", "trickle_trigger", "register_block_write"]), help=('Strips command to run'), type=str)
  parser.add_argument('-bc', '--bc', help='bunch counter for fast command and reset', default=0, required=False, type=int)
  parser.add_argument('-fd', '--fdaq_duration', help='For how many seconds fdaq command should record data (default 3)', default=3, required=False, type=int)
  parser.add_argument('-sd', '--send_delay', help='For how many seconds to wait after issuing fdaq command before sending register read or L0A command (default 1.0)', default=1.0, required=False, type=float)
  parser.add_argument('-fc', '--fast_command_id', help='ID of the fast command to send (default 0)', default="0", required=False, type=str)
  parser.add_argument('-hi', '--hcc_id', help='HCC ID (default broadcast)', default="0b1111", required=False, type=str)
  parser.add_argument('-ai', '--abc_id', help='ABC ID (default broadcast)', default="0b1111", required=False, type=str)
  parser.add_argument('-ri', '--register_id', help='Register ID (default 0)', default="0", required=False, type=str)
  parser.add_argument('-rd', '--register_data', help='Data to write to the register (default 0)', default="0", required=False, type=str)
  parser.add_argument('-d', '--felix_device', help='FELIX device ID (default 0)', default="0", required=False, type=str)
  parser.add_argument('-e_dout', '--elink_dout', help='ToHost elink ID where the Strips data is received from (default 00)', default="00", required=False, type=str)
  parser.add_argument('-e_lcb', '--elink_LCB', help='FromHost elink ID connected to LCB (default 00)', default="00", required=False, type=str)
  parser.add_argument('-e_r3l1', '--elink_R3_L1', help='FromHost elink ID connected to R3_L1 (default 00)', default="00", required=False, type=str)
  parser.add_argument('-p', '--path_to_ftools', help='Specify path to the ftools (fupload, fdaq, fcheck)', default="", required=False, type=str)
  parser.add_argument('-t', '--target', help='Register read/write target (ABC or HCC, default HCC)', default="HCC", required=False, type=str)
  parser.add_argument('-v', '--verbose', help='Verbose output (-vv = even more verbose)', action="count", default=0)
  parser.add_argument('-nfh', '--nibble_order_from_host', help='Nibble order from host (inverse or direct, default inverse)', default="inverse", required=False, type=str)
  parser.add_argument('-nth', '--nibble_order_to_host', help='Nibble order to host (inverse or direct, default direct)', default="direct", required=False, type=str)
  parser.add_argument('-dr', '--dry_run', help='Dry run: no actual commands or data are sent', action="store_true")
  parser.add_argument('-bcr', '--bcr', help='Set bunch counter reset in L0A packet (no BCR by default)', action="store_true")
  parser.add_argument('-m', '--mask', help='L0A mask (defalt 0b1111)', default="0b1111", required=False, type=str)
  parser.add_argument('-tg', '--tag', help='L0A tag (defalt 0x1A)', default="0x1A", required=False, type=str)
  parser.add_argument('-drv', '--driver', help='ITk Strips driver version (software, firmware, trickle), default software', default="software", required=False, type=str)
  parser.add_argument('-se', '--serial', help='ASIC serial for dynamic addressing (defalt 0)', default="0", required=False, type=str)
  parser.add_argument('-R', '--reset', help='Pass "-R" flag when calling fdaq/fdaqm', action="store_true")
  parser.add_argument('-ipp', '--ignore_physics_packets', help='Parser skips physics packets (use if the data coming from HCC*/ABC* is heavily corrupt)', action="store_true")
  parser.add_argument('-ld', '--l0a_delay', help='For how many seconds to wait between the lone BCR and L0A (default 2)', default=2, required=False, type=int)
  parser.add_argument('-de', '--descriptor', help='FELIX ToHost descriptor number', default=0, required=False, type=int)
  parser.add_argument('-rdl', '--register_data_list', help='Data list to write to the register (default [0])', default="0", required=False, nargs="+")
  parser.add_argument('-bl', '--block_length', help='Block length of block read command', default="1", required=False, type=str)

  global args
  args = parser.parse_args()

  driver = get_driver(args.driver)(dry_run=args.dry_run, target=args.target,
    elink_LCB=args.elink_LCB, elink_R3_L1=args.elink_R3_L1, elink_dout=args.elink_dout,
    hcc_id=args.hcc_id, abc_id=args.abc_id, felix_device=args.felix_device,
    path_to_ftools=args.path_to_ftools,
    verbose=args.verbose, bc=args.bc, fdaq_duration=args.fdaq_duration,
    nibble_order_to_host=args.nibble_order_to_host,
    nibble_order_from_host=args.nibble_order_from_host, raise_on_error=True,
    delay_before_send=args.send_delay, reset=args.reset, 
    ignore_physics_packets=args.ignore_physics_packets,
    descriptor=args.descriptor)

  if is_register_read(args.command):
    read_register(driver, args.register_id)

  elif args.command == 'listen':
    listen(driver)

  elif is_enable_abc(args.command):
    enable_abc(driver)

  elif is_configure_abc(args.command):
    driver.configure_ABC_star()

  elif is_disable_hpr(args.command):
    driver.write_register(16, 1, target="HCC")
    driver.write_register(0, 0b100, target="ABC")

  elif is_register_write(args.command):
    driver.write_register(args.register_id, args.register_data)

  elif is_listen_write(args.command):
    driver.write_register(args.register_id, args.register_data, listen=True)
    hpr_report_helper(driver)

  elif args.command == 'reset':
    driver.full_reset()

  elif args.command == 'fast':
    driver.fast_command(args.fast_command_id)

  elif is_l0a(args.command):
    driver.L0A(args.bcr, args.mask, args.tag)

  elif is_calibration_test(args.command):
    # disable HPR registers
    driver.write_register(16, 1, target="HCC")
    driver.write_register(0, 0b100, target="ABC")
    driver.fast_command(15) # Start PR & LP packets to ABCStar ASIC
    driver.fast_command(7) # ABC Hit counter reset
    driver.fast_command(8) # ABC Hit counter start
    driver.L0A(bcr=1, mask=0, tag=0) # send lonely BCR
    sleep(args.l0a_delay)
    driver.L0A_and_listen(args.bcr, args.mask, args.tag)
    driver.fast_command(9) # Hit counter stop
    driver.fast_command(11) # Stop PR & LP packets
    driver.verbose = 1
    driver.report_physics_packets()    

  elif (args.command == 'set_hcc_id'):
    driver.set_hcc_id(args.hcc_id, args.serial)  

# Special commands supported only with -drv firmware
  elif is_regmap_write(args.command):
    check_trickle_support(driver)
    driver.write_local_register(args.register_id, args.register_data)

  elif is_trickle_trigger(args.command):
    check_trickle_support(driver)
    driver.issue_trickle_trigger_pulse()

  elif args.command == "register_block_write":
    driver.write_register_block(register_id=args.register_id, register_data=args.register_data_list,
      block_length=len(args.register_data_list), generator=driver.generate_block_write)

  else:
    parser.error("Unsupported command: {}".format(args.command))

  if driver.hcc_parser != None:
    driver.report_parser_status()
    
  return 0


# get the spelling out of the way

def check_trickle_support(driver):
  if not args.driver == "firmware":
      raise TypeError("Unsupported driver type (required -drv firmware)")

def is_register_read(cmd):
  cmd = cmd.lower()
  return ((cmd == 'reg_read') or (cmd == 'register_read') or
    (cmd == 'read_reg') or (cmd == 'read_register') or (cmd == 'read'))

def is_register_write(cmd):
  cmd = cmd.lower()
  return ((cmd == 'reg_write') or (cmd == 'register_write') or
    (cmd == 'write_reg') or (cmd == 'write_register') or (cmd == 'write'))

def is_regmap_write(cmd):
  cmd = cmd.lower()
  return ((cmd == 'regmap_write') or (cmd == 'write_regmap') or
    (cmd == 'regmap') or (cmd == 'configure') or (cmd == 'config'))

def is_trickle_trigger(cmd):
  cmd = cmd.lower()
  return ((cmd == 'trickle_trigger') or (cmd == 'trigger_trickle') or
    (cmd == 'trigger'))

def is_listen_write(cmd):
  cmd = cmd.lower()
  return ((cmd == 'listen_write') or (cmd == 'write_listen'))

def is_enable_abc(cmd):
  cmd = cmd.lower()
  return ((cmd == 'enable_abc') or (cmd == 'abc_enable') or
    (cmd == 'abc_en') or (cmd == 'en_abc'))

def is_configure_abc(cmd):
  cmd = cmd.lower()
  return ((cmd == 'configure_abc') or (cmd == 'abc_cfg') or 
    (cmd == 'cfg_abc'))

def is_disable_hpr(cmd):
  cmd = cmd.lower()
  return ((cmd == 'disable_hpr') or (cmd == 'hpr_disable'))

def is_l0a(cmd):
  cmd = cmd.lower()
  return cmd == 'l0a'

def is_calibration_test(cmd):
  cmd = cmd.lower()
  return cmd == 'calibration' or cmd == 'calib_test' or cmd == 'cal_test'


# command handlers below 

def l0a(driver, bcr, mask, tag):
  driver.L0A(bcr, mask, tag)


def read_register(driver, register_id):
  driver.read_register(register_id)
  if driver.dry_run:
    return
  target = get_target_type(args.target)
  data = driver.hcc_parser.packet_map[target]
  errors = driver.hcc_parser.parse_errors
  timeout = driver.hcc_parser.parse_timeout

  print("- Register read data ({}*):".format(args.target))  
  hpr_report_helper(driver, verbose=args.verbose)
  if not data:
    print("(Could not find the register read response in the parser map)")
    if errors != 0 or timeout:
      print("Scanning HCC*/ABC* data for a specific packet with register_address={} (trying harder)".format(register_id))
      data = driver.find_register_read(register_id, target=args.target)
      if not data:
        print("Still can't find register read packets for register_address={}".format(register_id))      
  print(data)


def enable_abc(driver):
  driver.enable_ABC_star()
  sleep(1)
  driver.listen()
  hpr_report_helper(driver)


def listen(driver):
  driver.listen()
  hpr_report_helper(driver)


def hpr_report_helper(driver, verbose=1):
  if not driver.dry_run:
    driver.verbose = verbose
    driver.report_hcc_status()
    driver.report_abc_status()  


if __name__ == '__main__':
  try:
    main()
  except NameError as e:
    raise(e)
  except TypeError as e:
    raise(e)
  except Exception as e:
    print(e)
