# Purpose
This is a test suit for verifying FELIX firmware modified to support HCC* chips. The main test script verifies the basic communication automatically, while the tool scripts allow for sending individual commands and parsing the data from HCC* and ABC* in case if something doesn't work.


# System requirements:
- Python 3.6.8
- FELIX drivers and software (fdaq, fcheck, fupload)
- Special FELIX firmware version supporting HCC* chips

# Installation:
To install the scripts, copy and run these commands:

```
git clone ssh://git@gitlab.cern.ch:7999/ezhivun/hcc-star-python-readout.git
cd ./hcc-star-python-readout
python3 -m venv venv
source venv/bin/activate
pip3 install --upgrade pip
pip3 install -r requirements.txt
```


# Firmware

There are two major versions of FELIX firmware compatible with the ITk Strips:

- phase1: https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/tree/rm4.10_strips
- phase2: https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/tree/phase2/master

## Phase1

Phase1 variant is old, stable and is intended for early testing. It is missing latest features and there are discrepancies between phase2 and phase1 firmware.

__Please use phase2 firmware if at all possible.__

## Phase2

Phase2 is currently in development, and is the most up-to-date version. Phase1 and phase2 firmware require the correct corresponding version of FELIX software (fdaq, fcheck, fupload, etc) in order for the Python scripts to work.

To enable ToHost and FromHost elinks in phase2 firmware, run `phase2_strips_enable_test.sh`.

# Configure hardware 

## GBT test vehicle board (phase1 only)

- configure GBTx to supply clock from FELIX to HCCStar (160 MHz)
- CLK DES 7 / FREQ7 = 2

### GBT test vehicle board
FromHost elinks: 

- LCB command: 4 bit, direct mode (egroup 2, epath 0, elink: 0x10)
- LCB trickle: 4 bit, direct mode (egroup 0, epath 0, elink: 0x00)
- LCB trickle: 4 bit, direct mode (egroup 0, epath 2)
- R3L1: 4 bit, direct mode (egroup 2, epath 4)
- AMAC_OUT: 2 bit, direct mode (egroup 4, epath 0)

ToHost elink: 
- HCC_data: 8 bit, 8b10b mode (egroup 0 epath 5, elink: 0x05)
- AMAC_IN: 2 bit, direct mode (egroup 3, epath 4)

## Any lpGBT board (phase2 or phase1)

- Use FEC5, 10Gbps, transceiver mode
- Configure egroups 0-4 input 320 Mbps, output 160 Mbps

## phase1 elink mapping for other boards

Please refer to a json file corresponding to your build: https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/tree/rm4.10_strips/sources/templates/strips_links

### phase2 lpGBT elink mapping

For phase2 elink mapping, see section "LCB encoder" in https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/docs/FELIX_Phase2_firmware_specs.pdf 

To enable ToHost and FromHost elinks in phase2 firmware, run `phase2_strips_enable_test.sh`.

## lpGBT configuration

There are several lpGBT configuraiton found in `data/gbtxconfig` folder for testing strips modules. The configuration can be applied via USB-I2C dongle using `gbtxconfig` software, or via optical link using `fice` tool:

```
fice -G 0 -I 7F ./data/gbtxconfig/vldb_plus.txt
```

# How to use

Activate venv:
```
source venv/bin/activate
```

Run scripts:

```
./hcc_tool.py --help
./amac_tool.py --help
./parse_file.py --help
./run_all_tests.py --help
./probe_stave.py --help
./trickle.py --help
```

Leave venv:
```
deactivate
```

## Software and firmware LCB encoders

The `hcc_tool.py` and `run_all_tests.py` can encode the commands in "software" or "firmware" mode. The "software" mode will format commands as described in HCC* datasheet. The "firmware" mode will format commands for ITk Strips LCB firmware encoder (see https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/docs/FELIX_Phase2_firmware_specs.pdf for command format.)

You should always select the encoding mode compatible with the firmware and the type of Strips encoder elink. For example:

- `-drv software` = send data directly to elink, insert IDLE characters as appropriate. Script will generate commands in HCC* format, encode them in 6b8b.
    - Use `-nfh inverse` for:
        - Weihao's IDLE inserter firmware
    - Use `-nfh direct` for:
        - bypass link in phase1 firmware
        - R3L1 link in phase1 firmware
        - R3L1 command link in phase2 firmware
        - LCB command link in phase2 firmware when ENCODING_ENABLE=0 (default)
- `-drv firmware` = encode commands in LCB format for interpretation by the firmware. Use with:
    - LCB command and trickle elinks in phase1 firmware
    - trickle elink in phase2 firmware
    - LCB command link when ENCODING_ENABLE=1 in phase2 firmware


## Run pre-specified datapath tests - run_all_tests.py

Shell return code is 0 if all tests pass, 1 otherwise. Use `-v` flag for more detailed test reports, use `-vv` or `-vvv` to display debugging information. If tests are unstable, try increasing fdaq acquisition time (for example use `-fd 5`)

```
$ ./run_all_tests.py -e_lcb 10 -e_dout 05 -pe
Running: test_FromHost_elink_send_HCC_reset       [PASS]
Running: test_timeout_enabled                     [PASS]
Running: test_hcc_receive_hpr                     [PASS]
Running: test_hcc_register_write_disable_hpr      [PASS]
Running: test_hcc_register_read                   [PASS]
Running: test_hcc_register_read_write             [PASS]
Running: test_hcc_decode_errors                   [PASS]
Running: test_abc_star_enable                     [PASS]
Running: test_abc_register_write_disable_hpr      [PASS]
Running: test_abc_register_read                   [PASS]
-------------------------
Passed 10/10 tests in 141 seconds
```

(This feature is currently broken) The version with trickle configuration support will run additional tests for this feature:
```
Running: test_trickle_reg_read                    [PASS]
Running: test_trickle_retrigger                   [PASS]
Running: test_trickle_overwrite                   [PASS]
```

You can run these tests repeatedly and gather the pass/fail statistics using stability_test.sh script:
```
$ ./stability_test.sh 10 -e_dout 05 -e_lcb 10 -v
Prints '.' for an iteration that passed, 'x' for an iteration that failed:
Will run 10 iterations; each iteration can take a while
..........

Passed 10/10 test iterations
```

The output of the failed test iterations is saved to `stability_test_fails.log`

Known issues: sending "disable HPR packets" command might generate parse errors (hence the `-pe` flag)


## Run trickle configuration test - trickle.py

The trickle configuration test will fill trickle configuration memory with user-specified configuration, made of interleaved register write and read commands. The front-end generates response to the register read commands, which can then be captured by e.g. `fdaq` to verify whether the front-end is actually receiving the trickle configuration commands.

There are several pre-specified configurations stored in:

- `./data/r203_test_trickle_max_rate` - single segment, all register commands are broadcast
- `./data/stave_test_trickle_2021_05` - four segments with separate configurations on each, matching long stave
- See section "Creating custom trickle configuration" for making your own configuration

### How to run a trickle configuration test

Upload the trickle configuration to the firmware memory:
```
$ ./trickle.py -src ./data/r203_test_trickle_max_rate/ upload
Loading trickle configuration memory from ./data/r203_test_trickle_max_rate/
segment0_trickle.txt -> segment 0 (300 bytes total)
```

Enable ABC* chips and ABC* read function:
```
$ ./trickle.py -src ./data/r203_test_trickle_max_rate/ enable_abc
Configuring ABC* on FromHost elink 001
Configuring ABC* on FromHost elink 006
Configuring ABC* on FromHost elink 00b
Configuring ABC* on FromHost elink 010
```

Enable trickle configuration: commands stored in trickle configuration memory will now be sent continuously to the front-end.
```
$ ./trickle.py -src ./data/r203_test_trickle_max_rate/ start
```

Capture and analyze the "register read" responses from the front-end. WARNING: the analysis is currently implemented in Python, and it's very slow. Processing 1s of data from 1 HCC\* + 1 ABC\* takes about 5 minutes. This will be fixed in an future update.

```
$ ./trickle.py listen
Capturing FELIX data sample (device #0 descriptor #0)
Counting register_read responses on ToHost elink 000
Counting register_read responses on ToHost elink 002
Counting register_read responses on ToHost elink 004
Counting register_read responses on ToHost elink 006
Counting register_read responses on ToHost elink 008
Counting register_read responses on ToHost elink 00a
Counting register_read responses on ToHost elink 00c
Counting register_read responses on ToHost elink 00e
Counting register_read responses on ToHost elink 010
Counting register_read responses on ToHost elink 012
Counting register_read responses on ToHost elink 014
Counting register_read responses on ToHost elink 016
Counting register_read responses on ToHost elink 018
Counting register_read responses on ToHost elink 01a
 
Reg. ID \ RX elink    000
--------------------  --------
20                    02200000
21                    00000000
22                    00000000
23                    00ff3b05
24                    00000000
26                    0fffffff
27                    00000014
28                    000003ff
29                    00020001
2a                    00020001
2b                    00000000
2d                    00710003
2e                    00710003
2f                    00000000
30                    00406600
 
ABC* connected to RX elink 000
  Reg. ID \ ABC* ID  0
-------------------  --------
                 01  060d0f0d
                 02  100f100d
                 04  0000016c
                 06  0002000c
                 07  80070000
                 21  00000004
                 22  41800003
                 23  00042000
                 25  00000000
                 26  00000086
```

Additional csv and json reports are saved to ./data/reports. In json reports, "8" corresponds to HCC\* registers, and "4" to the ABC\* register. The "consistent" flag means that the register returned the same value for each readout.
```
{
    "8": {
        "000": {
            "2e": {
                "count": 3,
                "data": "00710003",
                "consistent": true
            },
            "2f": {
                "count": 3,
                "data": "00000000",
                "consistent": true
            },
            "30": {
                "count": 3,
                "data": "00406600",
                "consistent": true
            },
            "20": {
                "count": 2,
                "data": "02200000",
                "consistent": true
            },
            "21": {
                "count": 2,
                "data": "00000000",
                "consistent": true
                ...
```


Disable trickle configuration.
```
$ ./trickle.py -src ./data/r203_test_trickle_max_rate/ stop
```


### Creating custom trickle configurations for the test

To start, create a folder that will contain the trickle configuration data and create index.json inside that folder:
```
mkdir ./data/trickle_trigger_config
cd ./data/trickle_trigger_config
touch index.json
```

index.json will contain the information about the input data files and format, as well as stave segments and mapping.

The example `index.json` file, used with the long strips stave:

```
{
    "comment" : "Test dataset for Stefania's long stave, master only",
    "input" : {
        "segment_map" : {
            "0" : 3,
            "1" : 3,
            "2" : 3,
            "3" : 3,
            "4" : 1,
            "5" : 1,
            "6" : 1,
            "7" : 1,
            "8" : 2,
            "9" : 2,
            "a" : 2,
            "b" : 2,
            "c" : 2,
            "d" : 0
        },

        "data" : [
            {
                "parser" : "ItsdaqSummaryHCC",
                "file"   : "itsdaq_hcc_summary.txt",
                "config" : {
                    "for_each" : ["RegisterWrite", "RegisterRead", "Idle"],
                    "before_all" : ["DecodeErrorReadHCC", "Idle", "DecodeErrorReadABC", "Idle"],
                    "idles_each" : 10
                }
            },
            {
                "parser" : "ItsdaqSummaryABC",
                "file"   : "itsdaq_abc_summary.txt",
                "config" : {
                    "for_each" : ["RegisterWrite", "RegisterRead", "Idle"],
                    "idles_each" : 10
                }
            }           
        ]
    },

    "output" : [
        {
            "segment"   : 0,
            "file"      : "segment0_trickle.txt"
        },
        {
            "segment"   : 1,
            "file"      : "segment1_trickle.txt"
        },
        {
            "segment"   : 2,
            "file"      : "segment2_trickle.txt"
        },
        {
            "segment"   : 3,
            "file"      : "segment3_trickle.txt"
        }
    ]
}
```

In this file, the input section contains the segment map and the data file list.

The segment map describes which stave segment each HCC_ID is connected to. In this example, HCC\* with ID=0,1,2,3 are connected to segment 3, 4,5,6,7 are connected to segment 1, 8,9,a,b,c, are connected to segment 2, and d is connected to segment 0.

The data section contains the list of input file names and which parser is used to read each file. For example, in `ItsdaqSummaryHCC` format the registers are listed as a table, where column corresponds to HCC_ID from left (0x0) to right (0xD):
```
Register 32:     Delay1 02200000 02200000 02200000 02200000 02200000 02200000 02200000 02200000 02200000 02200000 02200000 02200000 02200000 02200000
Register 33:     Delay2 44444444 44444444 44444444 44444444 44444444 44444444 44444444 44444444 44444444 44444444 44444444 44444444 44444444 44444444
Register 34:     Delay3 00000444 00000444 00000444 00000444 00000444 00000444 00000444 00000444 00000444 00000444 00000444 00000444 00000444 00000444
Register 35:       PLL1 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05 00ff3b05
Register 36:       PLL2 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
```

Another format is `ItsdaqSummaryBroadcastHCC`, which is similar to `ItsdaqSummaryHCC`, but only has a single column that corresponds to broadcast commands. `ItsdaqSummaryABC` format is the same as `ItsdaqSummaryHCC`, but the parser will generate ABC\* read/write commands instead of HCC\*.

To see all available parsers, and add new custom ones, add a new class to `trickle/parsers.py`. 

`config` parameter specifies more details of forming commands in trickle configuration memory. For example,
```
"for_each" : ["RegisterWrite", "RegisterRead", "Idle"],
```
means that for each specified register, first `RegisterWrite` command will be issued, then `RegisterRead`, then a sequence of IDLE frames. The number of IDLE frames is specified in `"idles_each" : 10`.
```
"before_all" : ["DecodeErrorReadHCC", "Idle", "DecodeErrorReadABC", "Idle"],
```
This means that before processing all commands in the specified file, the followng sequence of commands will be inserted into trickle memory. `DecodeErrorReadHCC` adds "read HCC\* register 0x04 command (broadcast)". This register contains `LCBError` count that indicates how many LCB errors HCC\* has encountered. Similarly, `DecodeErrorReadABC` adds "read ABC\* register 0x34 command (broadcast)", containing the LCB error count for ABC\*. `Idle` inserts a sequence of IDLE frames, exactly the same as the `for_each` section.

The output section describes file names where the trickle data is stored for each stave segment. The data format is compatible with `fupload`:
```
0xa3 0x02 0x20 0x00 0x00 0x20 0xff 0xa1 0x20 0xff 
...
0xa2 0x00 0x00 0x00 0x86 0x26 0xff 0xa0 0x26 0xff 
```

You can generate the output files without uploading them by using the command:
```
$ ./trickle.py -src ./data/r203_test_trickle_max_rate/ compile
Successfully prepared trickle configuration contents in ./data/r203_test_trickle_max_rate/
```
The trickle configuration output files are also re-generated each time `trickle.py` `upload` command is used.

## AMAC configuration - amac_tool.py

This script can read and write individual AMAC registers, and send SETID command to the  AMAC chip.

To print full list of acceptable parameters:

```
./amac_tool.py --help
```

Use `-p` flag to specify path to ftools (fdaq, fcheck, fup) if not in $PATH.

Acceptable formats for the numericis are for example: 42 = decimal, 0b101010 = binary, 0x2A = hex, etc.

Use `-v` flag to increase the output verbosity, use -vv for even more verbose output.

Set AMAC ID = 7 of the chip with efuseid=0xbabac:
```
./amac_tool.py -ai 0x07 setid -ei 0xbabac
```

Set AMAC ID = 7 of the chip with idpads=0x05:
```
./amac_tool.py -ai 0x07 setid -pi 0x05
```

Read register 0x99 from AMAC with ID=5:
```
./amac_tool.py -ai 0x05 read -ri 0x99
```

Read next address:
```
./amac_tool.py -ai 0x05 next
```

Write 0x87654321 to address 0x05 of AMAC with ID=5:
```
./amac_tool.py -ai 0x05 write -ri 0x05 -rd 0x87654321
```

Send default AMAC configuration to AMAC with ID=5:
```
./amac_tool.py -ai 0x05 configure
```

## HCC\* and ABC\* configuration - hcc_tool.py

### HCC\* register reading and writing, sending fast commands

This script can issue individual HCC* and ABC* commands, such as register reading and writing, sending fast commands and L0A frames. Firmware driver also supports writing trickle configuration memory and configuring LCB link parameters.

To print full list of acceptable parameters:

```
./hcc_tool.py --help
```

Use `-p` flag to specify path to ftools (fdaq, fcheck, fup) if not in $PATH.

Acceptable formats for the numericis are for example: 42 = decimal, 0b101010 = binary, 0x2A = hex, etc.

Use `-v` flag to increase the output verbosity, use `-vv` for even more verbose output.

HCC* reset (sends ABC* register reset, HCC* logic reset, HCC* register reset)
```
./hcc_tool.py reset -v -e_lcb 10
```

Inspect data coming from the chip (such as HPR packets) without sending commands:
```
./hcc_tool.py -e_dout 05 -v listen
```

Fast command:
```
./hcc_tool.py fast -fc 10 -e_lcb 10 # Send ABC slow command reset 
```

Register read example (read LCB, R3L1 RawFrame):
```
./hcc_tool.py reg_read -v -ri 3 -e_lcb 10 -e_dout 05
```

Register write example (disable ToHost 8b10b encoding):
```
./hcc_tool.py reg_write -v -ri 42 -rd 0x10001 -e_lcb 10
./hcc_tool.py reg_write -v -ri 41 -rd 0x10001 -e_lcb 10
```

Configure the LCB link (set the address of the last valid byte in trickle configuration memory):
```
./hcc_tool.py -drv firmware configure -ri 0x0A -rd 2 -e_lcb 10
```

Enable ToHost 8b10b encoding:
```
./hcc_tool.py reg_write -v -ri 42 -rd 0x20001 -e_lcb 10
./hcc_tool.py reg_write -v -ri 41 -rd 0x20001 -e_lcb 10
```

Stop sending HCC* high-priority register read packets:
```
./hcc_tool.py reg_write -ri 16 -rd 1 -e_lcb 10
```

Clear the LCB and R3_L1 error counters:
```
./hcc_tool.py reg_write -ri 16 -rd 0b11000 -e_lcb 10
```

### Static and dynamic addressing support for HCC*

By default, hcc_tool.py and run_all_tests.py will issue broadcast commands (hcc_id=0b1111, abc_id=0b1111). Individual chips can be addressed using the parameters `--hcc_id=[address]`, `--abc_id=[address]`.

To configure HCC_id using eFuse serial number, run the command:
```
./hcc_tool.py set_hcc_id -hi (HCC* id) -se (Fuse ID) -v -e_lcb 10
```

The same parameters work with `./run_all_tests.py` script:
```
./run_all_tests.py -hi (HCC* id) -se (Fuse ID) -v -e_lcb 10 -e_dout 05
```
In this case, the configuration will be performed during test_FromHost_elink_send_HCC_reset, after sending the reset command.


### ABC* communication

Enable ABC* :
```
./hcc_tool.py -e_lcb 10 enable_abc
```

To disable ABC* HPR packets:
```
./hcc_tool.py -e_lcb 10 -v reg_write -ri 0 -rd 0b100 -t ABC
```
Known issues: sending this command might generate parse errors when parsing ToHost data

To enable ABC* HPR packets again:
```
./hcc_tool.py -e_lcb 10 fast -fc 3
```

To enable Read Register function of ABC*
```
./hcc_tool.py -v reg_write -ri 0x20 -rd 0x400 -t ABC -e_lcb 10
``` 

To read ABC* register:
```
./hcc_tool.py -v reg_read -ri 0x32 -t ABC -e_lcb 10 -e_dout 05
```

## Parse saved data stream - parse_file.py

To parse a file from fdaq:

```
fcheck -F 66666 -w file_from_fdaq.dat
./parse_file.py dataout.dat | less
```

## Find HCC\* mapping of a Star Chip Stave - probe_stave.py

To find how HCC* chips are connected to the stave (default is for lpGBT-phase1 firmware, lpGBT 0):
```
./probe_stave.py | tee stave_report.txt
```
You may specify the lpGBT optical link number to scan (default is to scan lpGBT 0 only).

```
./probe_stave.py --lpgbt 0
./probe_stave.py --lpgbt 1
```
An additional csv report will be saved in ./data/reports

Use parameters `-fh` and `-th` to select specific FromHost and ToHost elinks to probe.
```
./probe_stave.py -t -fh 018,01a,01c,01e -th 001,005,009,00d,011,015,019,01d,041,045,049,04d,051,055,059,05d | tee stave_report.txt
```

Use `-t` to run communication tests for each HCC* found on the stave. Please note that this process is very inefficient and testing one stave side may take up 30 minutes or more.
```
./probe_stave.py -t --lpgbt 0
```


# Help and support

Elena Zhivun - ezhivun@bnl.gov
