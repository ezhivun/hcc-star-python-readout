#!/bin/bash

echo ""
echo "This is a helper script for testing whether the LCB link receives the"
echo "trickle and bypass data from the AUX links. Before starting, make sure"
echo "enable the corresponding elinks anc configure them to 4-bit direct mode."
echo ""
echo "Please also load ILAs of the LCB link you would like to test and set up"
echo "the triggers."
echo "-----------------------------------------------------------------------"
echo "Press enter to continue"

read tmp

echo "LCB is instantiated at the elink (hex) [input and press enter]:"
read lcb_link

echo "LCB trickle data is received at the elink (hex) [input and press enter]:"
read trickle_link

echo "LCB bypass data is received at the elink (hex) [input and press enter]:"
read bypass_link

echo "(1/5) Press Enter to send fast command reset through LCB elink"
read tmp
./hcc_tool.py -e_lcb $lcb_link -drv firmware fast -fc 2 -v

echo "(2/5) Press Enter to send fast command reset through LCB BYPASS elink"
read tmp
./hcc_tool.py -e_lcb $bypass_link -drv software fast -nfh direct -fc 2 -v

echo "(3/5) Press Enter to configure LCB link before trickle configuration"
read tmp
./hcc_tool.py -e_lcb $lcb_link -drv firmware configure -ri 0x09 -rd 0 -v
./hcc_tool.py -e_lcb $lcb_link -drv firmware configure -ri 0x0A -rd 2 -v
./hcc_tool.py -e_lcb $lcb_link -drv firmware configure -ri 0x0B -rd 0 -v
./hcc_tool.py -e_lcb $lcb_link -drv firmware configure -ri 0x0C -rd 1 -v

echo "(4/5) Press Enter to save fast command reset to LCB trickle configuration"
read tmp
./hcc_tool.py -e_lcb $trickle_link -drv firmware fast -fc 2 -v

echo "(5/5) Press Enter to issue software trickle trigger"
read tmp
./hcc_tool.py -e_lcb $lcb_link -drv firmware trickle_trigger -v
