from bitstring import BitArray, BitStream

# command codes for firmware encoding
abc_read = "a0"
hcc_read = "a1"
abc_write = "a2"
hcc_write = "a3"
idle_enqueue = "80"


# encode firmware commands for the trickle configuration
class Encoder():
	__author__ = "Elena Zhivun"
	__version__ = "0.0.1"
	__maintainer__ = "Elena Zhivun"
	__email__ = "ezhivun@bnl.gov"


	def __init__(self, config):
		self.config = config

	# encode register write command
	def RegisterWrite(self, command):	
		command = Encoder.__FixFields(command)
		if command["type"].lower() == "abc":
			cmd=abc_write
		else:
			cmd=hcc_write			

		return (BitArray(hex=cmd) +
				BitArray(hex=command["reg_data"]) + 
				BitArray(hex=command["reg_id"]) + 
				BitArray(hex=command["hcc_id"]) + 
				BitArray(hex=command["abc_id"]))


	# encode register read command
	def RegisterRead(self, command):
		command = Encoder.__FixFields(command)
		if command["type"].lower() == "abc":
			cmd=abc_read
		else:
			cmd=hcc_read

		return (BitArray(hex=cmd) + 
				BitArray(hex=command["reg_id"]) +
				BitArray(hex=command["hcc_id"]) + 
				BitArray(hex=command["abc_id"]))


	# return a requested number of IDLE frames
	def Idle(self, command={}):
		cmd = idle_enqueue
		if not ("idles_each" in self.config):
			idles = 1
		else:
			idles = self.config["idles_each"]

		if idles <= 1:
			return BitArray(hex=cmd)

		cmd_array = [BitArray(hex=cmd)]*idles
		return BitArray().join(cmd_array)

	# Read the number of LCB decode errors form HCC
	def DecodeErrorReadHCC(self, **kwargs):
		command = {
			"reg_id" 	: "04",
			"name"		: "LCBErr",
			"hcc_id"	: "f", 	
			"abc_id"	: "f" 	
		}

		return (BitArray(hex=hcc_read) + 
				BitArray(hex=command["reg_id"]) +
				BitArray(hex=command["hcc_id"]) + 
				BitArray(hex=command["abc_id"]))


	# Read the number of LCB decode errors form ABC
	def DecodeErrorReadABC(self, **kwargs):
		command = {
			"reg_id" 	: "34",
			"name"		: "LCBErr",
			"hcc_id"	: "f", 	
			"abc_id"	: "f" 	
		}

		return (BitArray(hex=abc_read) + 
				BitArray(hex=command["reg_id"]) +
				BitArray(hex=command["hcc_id"]) + 
				BitArray(hex=command["abc_id"]))


	def do_action(self, action, **kwargs):
		func = getattr(self, action)
		return func(**kwargs)


	# adjust width of data and reg_id fields if they are too short
	def __FixFields(command):
		reg_data = command["reg_data"].strip()
		reg_id = command["reg_id"].strip()

		if len(reg_id) != 2:
			reg_id = "0" + reg_id

		if len(reg_data) != 8:
			reg_data = "0" * (8 - len(reg_data)) + reg_data

		command["reg_data"] = reg_data
		command["reg_id"] = reg_id		
		return command
