#!/usr/bin/env python3

import random
from os import system
from pathlib import Path
import signal
import sys

__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"

fast_command_inputs = "fast_command_inputs.txt"
fast_command_outputs = "fast_command_outputs.txt"
register_command_inputs = "register_command_inputs.txt"
register_command_outputs = "register_command_outputs.txt"
l0a_command_inputs = "l0a_command_inputs.txt"
l0a_command_outputs = "l0a_command_outputs.txt"
r3l1_command_inputs = "r3l1_command_inputs.txt"
r3l1_command_outputs = "r3l1_command_outputs.txt"
hcc_mask_test_inputs = "hcc_mask_test_inputs.txt"
hcc_mask_test_outputs = "hcc_mask_test_outputs.txt"
abc_mask_test_inputs = "abc_mask_test_inputs.txt"
abc_mask_test_outputs = "abc_mask_test_outputs.txt"

# Handle Ctrl+C properly
def signal_handler(sig, frame):
    print('Test sample generation aborted [Ctrl+C]')
    sys.exit(-1)

# creates files with fast command samples, if they don't already exist
def generate_fast_commands(file_in, file_out, count=40):
  if Path(file_in).is_file() and Path(file_out).is_file():
    print("Fast commands: samples already exist")
  else:
    print("Fast commands: generating samples")
    for i in range(count):
      system(("./sim_test_maker.py -drv firmware -bc {} -fc {} fast " + 
        "-if {} -of {}").format(random.randint(0, 3), random.randint(2, 15),
        file_in, file_out))

# creates files with register read and write command samples, if they don't already exist
def generate_register_commands(file_in, file_out, count=40):
  targets = ["HCC", "ABC"]
  if Path(file_in).is_file() and Path(file_out).is_file():
    print("Register commands: samples already exist")
  else:
    print("Register commands: generating samples")
    for i in range(count):
      if random.randint(0, 1):
        # generate read command
        system(("./sim_test_maker.py -drv firmware -t {} -ri {}" +
          " -hi {} -ai {} read -if {} -of {}").format(
          targets[random.randint(0, 1)],
          random.randint(0, 2**8 - 1),
          random.randint(0, 15), random.randint(0, 15),
          file_in, file_out))
      else:
        # generate write command
        system(("./sim_test_maker.py -drv firmware -t {} -ri {} -rd {}" +
          " -hi {} -ai {} write -if {} -of {}").format(
          targets[random.randint(0, 1)],
          random.randint(0, 2**8 - 1),
          random.randint(0, 2**32 - 1),
          random.randint(0, 15), random.randint(0, 15),
          file_in, file_out))


# creates files with elink-originated L0A command samples, if they don't already exist
def generate_l0a_commands(file_in, file_out, count=40, drv="firmware"):
  bcr = ["", "-bcr"]
  if Path(file_in).is_file() and Path(file_out).is_file():
    print("L0A commands: samples already exist (drv={})".format(drv))
  else:    
    print("L0A commands: generating samples (drv={})".format(drv))
    for i in range(count):
      system(("./sim_test_maker.py -drv {} -tg {} -m {} {} l0a " + 
        "-if {} -of {}").format(drv, random.randint(0, 2**7-1), random.randint(1, 15),
        bcr[random.randint(0, 1)], file_in, file_out))

# creates files with fast command samples, if they don't already exist
def generate_r3l1_commands(file_in, file_out, count=100):
  if Path(file_in).is_file() and Path(file_out).is_file():
    print("R3L1 commands: samples already exist")
  else:
    print("R3L1 commands: generating samples")
    for i in range(count):
      if random.randint(0, 4):
        # generate R3 command
        system(("./sim_test_maker.py -drv plain_text -tg {} -m {} r3l1 " + 
          "-if {} -of {}").format(random.randint(0, 2**7-1),
          random.randint(1, 2**5-1), file_in, file_out))
      else:
        # generate L1 command
        system(("./sim_test_maker.py -drv plain_text -tg {} -m {} r3l1 " + 
          "-if {} -of {}").format(random.randint(0, 2**7-1), 0,
          file_in, file_out))

# creates files for HCC mask capability tests, if they don't already exist
def generate_hcc_mask_test(file_in, file_out, count=40):

  if Path(file_in).is_file() and Path(file_out).is_file():
    print("HCC* mask test: samples already exist")
  else:
    print("HCC* mask test: generating samples")
    for hcc_id in range(16):
      # generate HCC* register read command
      system(("./sim_test_maker.py -drv firmware -t {} -ri {}" +
        " -hi {} -ai {} read -if {} -of {}").format(
        "HCC", random.randint(0, 2**8 - 1),
        hcc_id, random.randint(0, 15), file_in, file_out))

      # generate ABC* register read command
      system(("./sim_test_maker.py -drv firmware -t {} -ri {}" +
        " -hi {} -ai {} read -if {} -of {}").format(
        "ABC", random.randint(0, 2**8 - 1),
        hcc_id, random.randint(0, 15), file_in, file_out))

      # generate HCC* register write command
      system(("./sim_test_maker.py -drv firmware -t {} -ri {} -rd {}" +
        " -hi {} -ai {} write -if {} -of {}").format(
        "HCC",
        random.randint(0, 2**8 - 1),
        random.randint(0, 2**32 - 1),
        hcc_id, random.randint(0, 15), file_in, file_out))

      # generate ABC* register write command
      system(("./sim_test_maker.py -drv firmware -t {} -ri {} -rd {}" +
        " -hi {} -ai {} write -if {} -of {}").format(
        "HCC",
        random.randint(0, 2**8 - 1),
        random.randint(0, 2**32 - 1),
        hcc_id, random.randint(0, 15), file_in, file_out))

# creates files for ABC* mask capability tests, if they don't already exist
def generate_abc_mask_test(file_in, file_out, count=40):

  if Path(file_in).is_file() and Path(file_out).is_file():
    print("ABC* mask test: samples already exist")
  else:
    print("ABC* mask test: generating samples")
    for hcc_id in range(15):
      for abc_id in range(16):
        # generate ABC* register read command
        system(("./sim_test_maker.py -drv firmware -t {} -ri {}" +
          " -hi {} -ai {} read -if {} -of {}").format(
          "ABC", random.randint(0, 2**8 - 1),
          hcc_id, abc_id, file_in, file_out))

        # generate ABC* register write command
        system(("./sim_test_maker.py -drv firmware -t {} -ri {} -rd {}" +
          " -hi {} -ai {} write -if {} -of {}").format(
          "ABC",
          random.randint(0, 2**8 - 1),
          random.randint(0, 2**32 - 1),
          hcc_id, abc_id, file_in, file_out))

# generates sample trickle configuration
def generate_trickle_config(file_in, file_out, count=40):
  targets = ["HCC", "ABC"]
  if Path(file_in).is_file() and Path(file_out).is_file():
    print("Trickle configuration: samples already exist ({})".format(file_in))
  else:
    print("Trickle configuration: generating samples ({})".format(file_in))
    for i in range(count):
      system(("./sim_test_maker.py -drv firmware -t {} -ri {} -rd {}" +
        " -hi {} -ai {} write -if {} -of {}").format(
        targets[random.randint(0, 1)],
        random.randint(0, 2**8 - 1),
        random.randint(0, 2**32 - 1),
        random.randint(0, 15), random.randint(0, 15),
        file_in, file_out))

# generates sample trickle configuration
def generate_calibration(file_in, file_out, wait_idle_count=10, l0a_count=5, idles_between_l0a=2):
  targets = ["HCC", "ABC"]
  if Path(file_in).is_file() and Path(file_out).is_file():
    print("Trickle calibration: samples already exist ({})".format(file_in))
  else:
    print("Trickle calibration: generating samples ({})".format(file_in))
    # send lone BCR
    system(("./sim_test_maker.py -drv firmware -tg 0 -m 15 -bcr l0a -if {} -of {}").format(file_in, file_out))
    # send DIGITAL_PULSE
    system(("./sim_test_maker.py -drv firmware -bc 0 -fc 6 fast -if {} -of {}").format(file_in, file_out))
    # wait for some BCs (by sending IDLEs)
    for i in range(wait_idle_count):
      system(("./sim_test_maker.py -drv firmware idle -if {} -of {}").format(file_in, file_out))
    # generate L0A sequence
    for i in range(l0a_count):
      system(("./sim_test_maker.py -drv firmware -tg {} -m 15 l0a -if {} -of {}").format(i, file_in, file_out))
      if not i == l0a_count-1:
        for i in range(idles_between_l0a):
          system(("./sim_test_maker.py -drv firmware idle -if {} -of {}").format(file_in, file_out))


def main():
  generate_fast_commands(file_in=fast_command_inputs, file_out=fast_command_outputs)
  generate_register_commands(file_in=register_command_inputs, file_out=register_command_outputs)
  generate_l0a_commands(file_in=("elink_" + l0a_command_inputs),
    file_out=("elink_" + l0a_command_outputs), drv="firmware")
  generate_l0a_commands(file_in=("ttc_" + l0a_command_inputs),
    file_out=("ttc_" + l0a_command_outputs), drv="plain_text", count=200)
  generate_r3l1_commands(file_in=r3l1_command_inputs, file_out=r3l1_command_outputs)
  generate_hcc_mask_test(file_in=hcc_mask_test_inputs, file_out=hcc_mask_test_outputs)
  generate_abc_mask_test(file_in=abc_mask_test_inputs, file_out=abc_mask_test_outputs)
  generate_trickle_config(file_in="trickle_config_inputs_1.txt", file_out="trickle_config_outputs_1.txt", count=15)
  generate_trickle_config(file_in="trickle_config_inputs_2.txt", file_out="trickle_config_outputs_2.txt", count=15)
  generate_calibration(file_in="trickle_calibration_inputs.txt", file_out="trickle_calibration_outputs.txt")


if __name__ == '__main__':
  signal.signal(signal.SIGINT, signal_handler)
  main()