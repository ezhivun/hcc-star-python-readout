#!/bin/bash

REGMAP=`flx-config get REG_MAP_VERSION | sed 's/REG_MAP_VERSION=0x//' | cut -c 1-1`

if [ $REGMAP -eq 5 ]
then
    AMAC_ELINK="054"
    PHASE=2
else    
    AMAC_ELINK="0bf"
    PHASE=1    
fi

echo "This is phase${PHASE} firmware, AMAC is at elink 0x${AMAC_ELINK}"    

AMAC_RANGE="14 27"

int_handler()
{
    printf "\e[0m\n"
    echo "Ctrl+C"
    printf "\n"
    kill $PPID
    exit 1
}
trap 'int_handler' INT

# Configure and power on every AMAC (lpGBT slave only)

BGs=(0x8E8D 0x8F8E 0x8F8E 0x8F8F 0x8F8F 0x8F8E 0x8F8E 0x8E8E 0x8F8D 0x8F8E 0x8F8E 0x8E8D 0x8F8D 0x8D8F 0x8F8F 0x8E8E 0x8F8D 0x8F8E 0x8F8F 0x8F8D 0x8E8D 0x8E8E 0x8E8D 0x8E8D 0x8E8C 0x8F8F 0x8F8D 0x8F8D)

for i in `seq ${AMAC_RANGE}`;
do
    pads_id=$(($i - 14 ))
    echo Configuring AMAC \#$i
    echo "    ./amac_tool.py -ai $i setid -pi $pads_id -e_out ${AMAC_ELINK}"
    ./amac_tool.py -ai $i setid -pi $pads_id -e_out ${AMAC_ELINK}
    ./amac_tool.py -ai $i setid -pi $pads_id -e_out ${AMAC_ELINK}
    echo "    ./amac_tool.py -ai $i configure -e_out ${AMAC_ELINK} -bg ${BGs[$i]}"
    ./amac_tool.py -ai $i configure -e_out ${AMAC_ELINK} -bg ${BGs[$i]}
done 



