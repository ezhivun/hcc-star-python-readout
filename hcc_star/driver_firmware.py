from bitstring import BitArray, BitStream
from .driver_base import HCCStarDriverBase
import os

L0A_FRAME_PHASE = 0x00
L0A_FRAME_DELAY = 0x01
TTC_L0A_ENABLE = 0x02
TTC_BCR_DELAY = 0x03
GATING_TTC_ENABLE = 0x04
GATING_BC_START = 0x05
GATING_BC_STOP = 0x06
TRICKLE_TRIGGER_PULSE = 0x07
TRICKLE_TRIGGER_RUN = 0x08
TRICKLE_DATA_START = 0x09
TRICKLE_DATA_END = 0x0A
TRICKLE_WRITE_ADDR = 0x0B
TRICKLE_SET_WRITE_ADDR_PULSE = 0x0C
ENCODING_ENABLE = 0x0D
HCC_MASK = 0x0E
ABC_MASK_0 = 0x0F
ABC_MASK_1 = 0x10
ABC_MASK_2 = 0x11
ABC_MASK_3 = 0x12
ABC_MASK_4 = 0x13
ABC_MASK_5 = 0x14
ABC_MASK_6 = 0x15
ABC_MASK_7 = 0x16
ABC_MASK_8 = 0x17
ABC_MASK_9 = 0x18
ABC_MASK_A = 0x19
ABC_MASK_B = 0x1A
ABC_MASK_C = 0x1B
ABC_MASK_D = 0x1C
ABC_MASK_E = 0x1D
ABC_MASK_F = 0x1E

elink_mapping = {
  "lcb_config" : [0, 5, 10, 15],
  "lcb_command" : [1, 6, 11, 16],
  "lcb_trickle" : [2, 7, 12, 17],
  "r3l1_config" : [3, 8, 13, 18],
  "r3l1_command" : [4, 9, 14, 19]
}


def get_driver(base_class):
  #print("Instantiating trickle driver with base class {}".format(base_class))
  # Handles the HCC* communication protocol - 6b8b encoding is done in firmware
  class HCCStarDriverFirmware(base_class):
    trickle_buffer = BitArray()

    def generate_write_command(self, hcc_id, abc_id, register_id, 
      register_data, target):
      opcode = '0xA3' if self.is_target_hcc(target) else '0xA2'
      return (BitArray(opcode) + BitArray(uint=register_data, length=32) 
        + BitArray(uint=register_id, length=8) + BitArray(uint=hcc_id, length=4)
        + BitArray(uint=abc_id, length=4))


    def generate_read_command(self, hcc_id, abc_id, register_id, target):
      opcode = '0xA1' if self.is_target_hcc(target) else '0xA0'
      return (BitArray(opcode) + BitArray(uint=register_id, length=8) +
        BitArray(uint=hcc_id, length=4) + BitArray(uint=abc_id, length=4))

    # returns the target if (ABC or HCC)
    def is_target_hcc(self, target):
      HCC = 'HCC'
      ABC = 'ABC'
      valid_targets = [HCC, ABC]
      if not target in valid_targets:
        raise Exception("Invalid target. Valid targets:" + str(valid_targets))
      return target == 'HCC'

    def generate_reset_command(self, bc):
      return [self.fast_command_cmd(bc, 3),
        self.fast_command_cmd(bc, 2),
        self.fast_command_cmd(bc, 12)]


    def generate_l0a_command(self, bcr, mask, tag):    
      return (BitArray('0x82') + BitArray('0b000') + BitArray(uint=bcr, length=1) +
        BitArray(uint=mask, length=4) + BitArray(uint=tag, length=8))
    

    def generate_fast_command(self, bc, command_id):    
      return self.fast_command_cmd(bc, command_id)


    def generate_r3l1_command(self, mask, tag):
      raise NameError("R3L1 command is not supported for this encoder (firmware)")


    def fast_command_cmd(self, bc, command_id):
      return (BitArray('0x81') + BitArray('0b00') + BitArray(uint=bc, length=2) +
        BitArray(uint=command_id, length=4))


    def generate_idle_command(self):    
      return BitArray('0x80')


    def generate_write_local_register_command(self, reg_id, reg_data):
      return (BitArray('0x10') + BitArray(uint=reg_data, length=16) +
        BitArray(uint=reg_id, length=8))

    # send IDLE packet
    def send_idle(self):
      command = self.generate_idle_command()
      if not command:
        raise ValueError("Driver returned an empty IDLE packet")
      self.send_command(command)
    
    # write local register map
    def write_local_register(self, reg_id, reg_data, listen=False, elink=None):    
      reg_id = self.to_integer(reg_id)
      reg_data = self.to_integer(reg_data)
      self.report(("Write local regmap register: reg_id={}, reg_data=0x{:08x}"
        ).format(reg_id, reg_data), verbosity=1)
      command = self.generate_write_local_register_command(reg_id, reg_data)
      if not command:
        raise ValueError("Driver returned an empty register map write command")
      if listen:
        self.listen_and_send_command(command, elink)
      else:
        self.send_command(command, elink)

    # issue a single trickle trigger pulse
    def issue_trickle_trigger_pulse(self, channel, listen=True):
      self.report("Issuing a single trickle trigger pulse", verbosity=1)
      self.write_local_register(TRICKLE_TRIGGER_PULSE, 1,
        listen=listen, elink=elink_mapping["lcb_config"][channel])


    # enable/disable continuous trickle configuration
    def trickle_trigger_run(self, run_enable, channel, listen=False):
      if run_enable:
        run_enable = 1
        status_word = "Enabling"
      else:
        run_enable = 0
        status_word = "Disabling"

      elink_config = "{0:02x}".format(elink_mapping["lcb_config"][channel])

      self.report(("{} continuous trickle configuration on channel {},"
        + " elink {} (TRICKLE_TRIGGER_RUN)").format(status_word, channel,
        elink_config), verbosity=1)
      self.write_local_register(TRICKLE_TRIGGER_RUN, run_enable,
        listen=listen, elink=elink_config)


    # update trickle configuration memory from file
    def load_trickle_memory_from_file(self, file_name, channel, length_bytes):
      self.report("Loading trickle configuration from {} in channel {} ({} bytes total)".format(
        file_name, channel, length_bytes), verbosity=1)
      elink_config = "{0:02x}".format(elink_mapping["lcb_config"][channel])
      elink_trickle = "{0:02x}".format(elink_mapping["lcb_trickle"][channel])
      self.write_local_register(TRICKLE_WRITE_ADDR, 0, elink=elink_config)
      self.write_local_register(TRICKLE_SET_WRITE_ADDR_PULSE, 1, elink=elink_config)
      self.send_file(file_name, elink=elink_trickle)
      self.write_local_register(TRICKLE_DATA_START, 0, elink=elink_config)
      self.write_local_register(TRICKLE_DATA_END, length_bytes, elink=elink_config)


# -------- experimental mini-sequencer commands  -----------

    def generate_write_like(self, hcc_id, abc_id, register_id, 
      register_data, opcode):
      return (BitArray(opcode) + BitArray(uint=register_data, length=32) 
        + BitArray(uint=register_id, length=8) + BitArray(uint=hcc_id, length=4)
        + BitArray(uint=abc_id, length=4))


    # data is an array of block_length
    def generate_block_write_like(self, hcc_id, abc_id, register_id, 
      block_length, register_data, opcode):
      data = (BitArray(opcode) + BitArray(uint=register_id, length=8)
        + BitArray(uint=hcc_id, length=4) + BitArray(uint=abc_id, length=4)
        + BitArray(uint=block_length-1, length=8))

      for i in range(block_length):
        data += BitArray(uint=register_data[i], length=32)

      return data


    def generate_block_read(self, hcc_id, abc_id, register_id, 
      block_length, target):
      opcode = '0xB1' if self.is_target_hcc(target) else '0xB0'
      return (BitArray(opcode) + BitArray(uint=register_id, length=8)
        + BitArray(uint=hcc_id, length=4) + BitArray(uint=abc_id, length=4)
        + BitArray(uint=block_length-1, length=8))


    # data is an array
    def generate_block_write(self, hcc_id, abc_id, register_id, 
      block_length, register_data, target):
      opcode = '0xB3' if self.is_target_hcc(target) else '0xB2'
      return self.generate_block_write_like(hcc_id, abc_id, register_id,
        block_length, register_data, opcode)


    def generate_read_write(self, hcc_id, abc_id, register_id, 
      register_data, target):
      opcode = '0xA5' if self.is_target_hcc(target) else '0xA4'
      return self.generate_write_like(hcc_id, abc_id, register_id,
        register_data, opcode)


    def generate_write_read(self, hcc_id, abc_id, register_id, 
      register_data, target):
      opcode = '0xA7' if self.is_target_hcc(target) else '0xA6'
      return self.generate_write_like(hcc_id, abc_id, register_id,
        register_data, opcode)


    def generate_block_read_write(self, hcc_id, abc_id, register_id,
      block_length, register_data, target):
      opcode = '0xB5' if self.is_target_hcc(target) else '0xB4'
      return self.generate_block_write_like(hcc_id, abc_id, register_id,
        block_length, register_data, opcode)


    # data is an array
    def generate_block_write_read(self, hcc_id, abc_id, register_id, 
      block_length, register_data, target):
      opcode = '0xB7' if self.is_target_hcc(target) else '0xB6'
      return self.generate_block_write_like(hcc_id, abc_id, register_id,
        block_length, register_data, opcode)

  return HCCStarDriverFirmware