from time import sleep
from termcolor import colored, cprint
from bitstring import BitArray

import os
import subprocess
import math

fup_file_name = 'fupload_cmd.txt'
fdaq_file_name = 'fdaq_data.dat'
fdaq_file_name_alt = 'fdaq_data-1.dat'
fdaq_file_name_phase2 = 'fdaq_data-1.dat{}{}'
fcheck_file_name = 'dataout.dat'

# This class encapsulates ftools system calls shared between other scripts
# along with some shared reporting functions
class FtoolsBase():

  def __init__(self):
    self.data_filename = fcheck_file_name

  # read a given register from FELIX register map
  # inputs e.g. "TIMEOUT_CTRL_ENABLE=0x1"
  # returns stuff to the right of '='
  def felix_config_get(self, reg_name):
    self.report("Read FELIX register {}".format(reg_name), verbosity=2)    
    self.issue_flx_config("get", reg_name)    
    if self.dry_run:
      if reg_name ==  "TIMEOUT_CTRL_ENABLE":
        return "0x1"
      elif reg_name ==  "REG_MAP_VERSION":      
        return "0x500"
      return "0x0"

    if self.process_result.returncode != 0:
      raise ValueError(self.process_result.stderr)
    self.report("{}".format(self.process_result.stdout), verbosity=2)
    out_split = self.process_result.stdout.split("=")
    return out_split[1]    

  # figure out which register map version we are running
  def check_regmap_version(self):    
    regmap_version = int(self.felix_config_get("REG_MAP_VERSION"), 0)
    self.regmap_major = regmap_version // 256
    self.regmap_minor = regmap_version % 256
    self.report("Firmware version {}.{:02}".format(self.regmap_major, self.regmap_minor), verbosity=1)


  # sends a pre-encoded command
  def send_command(self, command, elink=None):
    elink = elink or self.elink_out
    self.report("Send command to elink {}".format(elink), verbosity=2)
    if isinstance(command, list):
      command = BitArray().join(command)
    self.report_data_to_be_sent(command)
    self.issue_fupload(command, elink=elink)


  # sends a file to selected elink
  def send_file(self, file_name, elink=None):
    elink = elink or self.elink_out
    self.report("Sending file {} to elink {}".format(file_name, elink), verbosity=2)
    self.fupload_file(file_name, elink)


  # encodes and sends a command while listening for data from the chip
  def listen_and_send_command(self, command, elink_out=None):
    elink_out = elink_out or self.elink_out
    self.report("Listen, then send command to elink {}".format(elink_out), verbosity=2)
    self.issue_fdaq(self.fdaq_duration)
    delay_before_send = self.to_float(self.delay_before_send)
    if not self.dry_run: sleep(delay_before_send) 
    if command:
      self.send_command(command, elink_out) 
    if not self.dry_run: sleep(self.fdaq_duration - delay_before_send + 0.5)

    if self.regmap_major == 4:
      if os.path.isfile(fdaq_file_name): 
          fdaq_file = fdaq_file_name
      else:
        fdaq_file = fdaq_file_name_alt
    else:
      fdaq_file = fdaq_file_name_phase2.format(self.felix_device, self.descriptor)
    self.report("ToHost data is saved in {} file".format(fdaq_file), verbosity=2)

    if self.dry_run:
      self.block_count = 0
    else:
      stat = os.stat(fdaq_file)
      if not stat.st_size and self.raise_on_error:
        raise Exception("No data was received by fdaq from FELIX device {}".format(
          self.felix_device, self.elink_in))
      self.block_count = math.ceil(stat.st_size / 1024);

    self.fdaq_file = fdaq_file
    self.issue_fcheck()
    if self.dry_run: return

    stat = os.stat(fcheck_file_name)
    if not stat.st_size and self.raise_on_error:
      raise Exception("No data was received from FELIX device {} elink {}".format(
        self.felix_device, self.elink_in))

    self.parse_response(fcheck_file_name)

  def parse_response(self):
    pass

# prints data to be sent to the link
  def report_data_to_be_sent(self, command):
    self.report("Sending {}".format(command), verbosity=2)


  def clear_report(self):
    self.report("Clear report", verbosity=2)
    self.report_data = ""
    return

  # prints the parsed packet message (or doesn't if not verbose)
  def report(self, msg, verbosity=1):
    colors=['grey', 'white', 'cyan', 'green', 'blue' 'magenta']
    if self.verbose >= verbosity:
      for _ in range(verbosity):
        print("---", end='')
      cprint(msg, colors[verbosity])


# =============== private methods below ========================
# these calls should be replaced by more efficient FELIX python bindings
# for an order of magnitude performance boost
  
  def issue_fdaq(self, fdaq_acquisition_time):
    self.report("Issue fdaq command", verbosity=2)
    timeout = round(fdaq_acquisition_time) + 2
    reset_flag = "" if self.reset else "-R"    
    quiet = " 1>/dev/null" if self.verbose < 1 else ""
    if self.regmap_major == 4:
      # phase1 data readout
      fdaq_cmd = 'timeout {}s {} -d {} {} -T -t {} {} {} 2>&1 &'.format(
        timeout,
        os.path.join(self.path_to_ftools, 'fdaq'), 
        self.felix_device, 
        reset_flag,
        fdaq_acquisition_time,
        fdaq_file_name,
        quiet)
    else:
      # phase2 data readout, specify which descriptor to read from
      fdaq_cmd = 'timeout {}s {} -d {} {} -T -t {} -i {} {} {} 2>&1 &'.format(
        timeout,
        os.path.join(self.path_to_ftools, 'fdaqm'), 
        self.felix_device, 
        reset_flag,
        fdaq_acquisition_time,
        self.descriptor,
        fdaq_file_name,        
        quiet)
    self.report(fdaq_cmd)
    if self.dry_run:
      return
    # can't have error code for a bg process synchronously
    if self.verbose:
      self.process_result = subprocess.run(fdaq_cmd, shell=True)
    else:
      self.process_result = subprocess.run(fdaq_cmd, 
         shell=True, stdout=self.FNULL, stderr=self.FNULL)
    # self.report(self.process_result.stdout)
    #os.system(fdaq_cmd)


  def run_in_foreground_shell(self, cmd):
    self.report(cmd)
    if self.dry_run:
      return ""
    self.process_result = subprocess.run(cmd, check=self.raise_on_error,
      shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
      universal_newlines=True)
    self.report(self.process_result.stdout)
    return self.process_result.stdout


  def issue_flx_config(self, command, reg_name):
    self.report("Issue flx-config command", verbosity=2)
    cmd = '{} {} {} -d {}'.format(os.path.join(
      self.path_to_ftools, 'flx-config'), command, reg_name, self.felix_device)
    self.run_in_foreground_shell(cmd)


  def issue_fupload(self, command, elink=None):
    elink = elink or self.elink_out
    byte_count = 0
    self.report("Issue fupload command", verbosity=2)
    with open(fup_file_name, 'w') as f:
      for byte in command.cut(8):
        f.write("0x{:02x} ".format(byte.uint))
        byte_count += 1    
    cmd = '{} -d {} -b 1024 -c -e {} -r 1 {}'.format(os.path.join(
      self.path_to_ftools, 'fupload'), self.felix_device, elink, fup_file_name)    
    self.run_in_foreground_shell(cmd)
    self.report("Sent {} bytes to the Strips link".format(byte_count), verbosity=1)


  def fupload_file(self, file_name, elink=None):
    elink = elink or self.elink_out
    self.report("Send file {} to elink {} with fupload command".format(file_name, elink), verbosity=2)  
    cmd = '{} -d {} -b 1024 -c -e {} -r 1 {}'.format(os.path.join(
      self.path_to_ftools, 'fupload'), self.felix_device, elink, file_name)    
    self.run_in_foreground_shell(cmd)


  def issue_fcheck(self):
    self.fcheck_error = False
    self.report("Issue fcheck command (check for error blocks)", verbosity=2)
    cmd = '{} {}'.format(os.path.join(self.path_to_ftools, 'fcheck'), self.fdaq_file)
    result = self.run_in_foreground_shell(cmd)
    if "error" in result:
      self.report("fcheck detected error chunks", verbosity=1)
      self.fcheck_error = True

    self.report("Issue fcheck command (select blocks)", verbosity=2)
    cmd = '{} -e {} -F {} -w {}'.format(
      os.path.join(self.path_to_ftools, 'fcheck'), 
      self.elink_in, self.block_count, self.fdaq_file)
    self.run_in_foreground_shell(cmd)

  def no_data_received_from_elink(self):
    stat = os.stat(fcheck_file_name)
    return stat.st_size == 0

