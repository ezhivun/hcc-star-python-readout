from bitstring import BitArray, BitStream, Bits, ReadError
from bidict import bidict

# This class converts between 6b8b encoded and decoded words
# make this into a proper class with tests and all
# turn decode_array and decode_word into the same method
class Codec6b8b():
  # Bidirectional dictionary for 6b8b codec
  bidict_6b8b = bidict(
      {0b000000 : 0b01011001, # D00
       0b000001 : 0b01110001, # D01
       0b000010 : 0b01110010, # D02
       0b000100 : 0b01100101, # D04
       0b001000 : 0b01101001, # D10
       0b010000 : 0b01010011, # D20
       0b100000 : 0b01100011, # D40
       0b110000 : 0b01110100, # D60
       0b111111 : 0b01100110, # D77
       0b111110 : 0b01001110, # D76
       0b111101 : 0b01001101, # D75
       0b111011 : 0b01011010, # D73
       0b110111 : 0b01010110, # D67
       0b101111 : 0b01101100, # D57
       0b011111 : 0b01011100, # D37
       0b001111 : 0b01001011  # D17
    })

  kommas = [0b01000111,
            0b01100101,
            0b01111000,
            0b01101010]


  @staticmethod
  def encode_array(data_array):
    if not isinstance(data_array, BitArray):
      raise TypeError("data_array must be BitArray")
    encoded = [Codec6b8b.encode_word(data) for data in data_array.cut(6)]
    return BitArray().join(encoded)

  @staticmethod
  def decode_array(data_array):
    if not isinstance(data_array, BitArray):
      raise TypeError("data_array must be BitArray")
    decoded = [Codec6b8b.decode_word(data) for data in data_array.cut(8)]
    return BitArray().join(decoded)

  @staticmethod
  def encode_word(data):
    if not data.len == 6:
      raise TypeError("data must be 6 bits long")
    if data.uint in Codec6b8b.bidict_6b8b:
      encoded_uint = Codec6b8b.bidict_6b8b[data.uint]
      return BitArray(uint=encoded_uint, length=8)
    disparity = data.count(1) - data.count(0)
    if disparity == 2:
      return BitArray("0b00") + data
    elif disparity == -2:
      return BitArray("0b11") + data
    elif disparity == 0:
      return BitArray("0b10") + data
    raise Exception("Encoding error - incorrect disparity = {} (data = {})"
                    .format(disparity, data.bin))

  @staticmethod
  def decode_word(data):
    if not data.len == 8:
      raise TypeError("data must be 8 bits long (was {})".format(data.bin))
    disparity = data.count(1) - data.count(0)
    if not disparity == 0:
      raise Exception("Decoding error: {} is not a valid 6b8b word"
                      .format(data))
    if data.uint in Codec6b8b.bidict_6b8b.inverse:
      decoded_uint = Codec6b8b.bidict_6b8b.inverse[data.uint]
      return BitArray(uint=decoded_uint, length=6)
    if data.uint in Codec6b8b.kommas:
      raise Exception("Decoding error: comma character {}".format(data.bin))
    return data[2:8]

  # make this a proper test in the future
  @staticmethod
  def self_check():
    for i in range(64):
      try:
        # check that every number decodes to itself
        decoded = BitArray("0b0")
        data = BitArray(uint=i, length=6)
        encoded = Codec6b8b.encode_word(data)
        decoded = Codec6b8b.decode_word(encoded)
        if not decoded == data:
          print("Error encoding/decoding - expected {} was {} (encoded {})"
              .format(data.bin, decoded.bin, encoded.bin))
      except Exception as e:
        print(e)

    # check that the bitstring of all numbers decodes to itself
    original_array = [BitArray(uint=i, length=6) for i in range(64)]
    original = BitArray().join(original_array)
    encoded = Codec6b8b.encode_array(original)
    decoded = Codec6b8b.decode_array(encoded)
    if not original == decoded:
      print("Original and decoded full BitArrays don't match")
    return


# switches nibbles around in given bitarray
def swap_nibbles(data):
  if not isinstance(data, BitArray):
    raise TypeError("data must be BitArray")
  if data.length % 8:
    raise TypeError("The number of bits in data must be divisible by 8")
  swapped_data = BitArray(data.length)
  for i in range(0, data.length//8):
    swapped_data[8*i:8*i+4] = data[8*i+4:8*(i+1)]
    swapped_data[8*i+4:8*(i+1)] = data[8*i:8*i+4]
  return swapped_data
