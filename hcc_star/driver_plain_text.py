from bitstring import BitArray, BitStream
from .driver_base import HCCStarDriverBase


# Outputs input parameters as plain text (for VHDL testbench to parse)
# don't use with the actual HCC*/ABC* chips
def get_driver(base_class):

  class HCCStarDriverPlainText(base_class):

    def generate_write_command(self, hcc_id, abc_id, register_id, 
      register_data, target):
      target_select = self.get_target_select(target)
      return ("{:01b} -- ABC / not HCC\n{:04b} -- hcc_id\n{:04b}" + 
        " -- abc_id\n{:08b} -- register_id\n{:032b} -- data").format(
        target, hcc_id, abc_id, register_id, register_data)


    def generate_read_command(self, hcc_id, abc_id, register_id, target):
      target_select = self.get_target_select(target)
      return ("{:01b} -- ABC / not HCC\n{:04b} -- hcc_id\n{:04b}" + 
        " -- abc_id\n{:08b} -- register_id").format(
        target, hcc_id, abc_id, register_id, register_data)


    def generate_reset_command(self, bc):
      return ""


    def generate_l0a_command(self, bcr, mask, tag):    
      return "{:01b} -- BCR \n{:04b} -- mask\n{:07b} -- tag".format(bcr, mask, tag)


    def generate_r3l1_command(self, mask, tag):    
      return "{:05b} -- mask\n{:07b} -- tag".format(mask, tag)
    

    def generate_fast_command(self, bc, command_id):    
      return self.fast_command_cmd(bc, command_id)


    def fast_command_cmd(self, bc, command_id):
      return "{:02b} -- BC\n{:04b} -- command_id".format(bc, command_id)

    # prints data to be sent to the link
    def report_data_to_be_sent(self, command):
      self.report("Sending {}".format(command), verbosity=2)

  return HCCStarDriverPlainText