import hcc_star.driver_firmware as firmware
import hcc_star.driver_software as software
import hcc_star.driver_plain_text as plain_text
from .driver_base import HCCStarDriverBase

def get_driver(implementation, base_class=HCCStarDriverBase):
  if implementation == "software":
    return software.get_driver(base_class)
  elif implementation == "firmware":
    return firmware.get_driver(base_class)
  elif implementation == "plain_text":
    return plain_text.get_driver(base_class)
  else:
    raise ValueError("Unsupported driver implementation (supported: software, firmware, plain_text)")

