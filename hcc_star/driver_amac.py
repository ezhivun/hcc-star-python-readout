from .driver_ftools import FtoolsBase

from bitstring import BitArray, BitStream
from functools import reduce
from operator import xor

import os
import math


class DriverAMAC(FtoolsBase):
  def __init__(self, dry_run=False, elink_in="00", elink_out="00",
    felix_device="0", path_to_ftools="", verbose=0, fdaq_duration=2,
    raise_on_error=True, delay_before_send=1, reset=False, descriptor=0):

    self.dry_run = dry_run
    self.elink_in = elink_in
    self.elink_out = elink_out
    self.felix_device = felix_device
    self.path_to_ftools = path_to_ftools
    self.verbose = verbose
    self.fdaq_duration = fdaq_duration
    self.raise_on_error = raise_on_error
    self.delay_before_send = delay_before_send
    self.reset = reset # reset elink when running fdaq
    self.descriptor = descriptor

    self.report_data = ""
    self.response = {"comm_id" : None, "seq_id" : None, "register_data" : None}
    self.FNULL = open(os.devnull, 'w')
    self.hcc_parser = None

    self.check_regmap_version()


  # Read register data given AMAC ID and register address
  def read(self, amac_id, register_id):    
    register_id = self.to_integer(register_id)
    amac_id = self.to_integer(amac_id)
    self.report("Read register address=0x{:x}, AMAC_id=0b{:b}".format(
      register_id, amac_id), verbosity=2)
    command = (BitArray('0b101') + BitArray(uint=amac_id, length=5)
      + BitArray(uint=register_id, length=8))
    self.clear_report()
    self.command_type = "read"
    self.listen_and_send_command(command) 


  # Read register data from the next register
  def read_next(self, amac_id):
    amac_id = self.to_integer(amac_id)
    self.report("Read next register, AMAC_id=0b{:b}".format(amac_id), verbosity=2)
    command = BitArray('0b100') + BitArray(uint=amac_id, length=5)
    self.clear_report()
    self.command_type = "read"
    self.listen_and_send_command(command) 


  # Write a register given its ID and data (HCC* or ABC*)
  # (target="HCC" or "ABC"; if not provided the value is taken from the class)
  def write(self, amac_id, register_id, register_data, listen=False):
    amac_id = self.to_integer(amac_id)
    register_id = self.to_integer(register_id)
    register_data = self.to_integer(register_data)
    self.report("Write 0x{:x} to register 0x{:x}, AMAC_id=0b{:b}".format(
      register_data, register_id, amac_id), verbosity=2)
    self.clear_report()
    command = (BitArray('0b111') + BitArray(uint=amac_id, length=5)
      + BitArray(uint=register_id, length=8) + BitArray(uint=register_data, length=32))
    command = command + self.get_crc(command)
    if listen:
      self.command_type = "write"
      self.listen_and_send_command(command)
    else:
      self.send_command(command)

  # Set AMAC id after power-on reset
  def set_id(self, amac_id, id_pads, efuse_id=0b11111111111111111111, listen=False):
    amac_id = self.to_integer(amac_id)
    efuse_id = self.to_integer(efuse_id)
    id_pads = self.to_integer(id_pads)
    self.report("Set AMAC_id=0b{:b} (efuse_id=0b{:b}, id_pads=0x{:x})".format(
      amac_id, efuse_id, id_pads), verbosity=2)
    self.clear_report()
    command = (BitArray('0b110') + BitArray('0xFF') + BitArray(uint=amac_id, length=5)
      + BitArray('0xF') + BitArray(uint=efuse_id, length=20) + BitArray('0b111')
      + BitArray(uint=id_pads, length=5))
    command = command + self.get_crc(command)
    if listen:
      self.command_type = "write"
      self.listen_and_send_command(command)
    else:
      self.send_command(command)

  def configure(self, amac_id, bg):
    bg = self.to_integer(bg)
    self.report("Set BG=0x{:x})".format(bg), verbosity=2)
    self.write(amac_id, 40, 0x00077700)
    self.write(amac_id, 41, 0x00077700) 
    self.write(amac_id, 42, 1) # writeReg(tx, 42  ,  0x00000000); // DCDC disabled by default
    self.write(amac_id, 43, 1) # writeReg(tx, 43  ,  0x00000000); // DCDC disabled by default
    self.write(amac_id, 44, 0) # writeReg(tx, 44  ,  0x00000000); // no interlock or warnings enabled
    self.write(amac_id, 45, 0) # writeReg(tx, 45  ,  0x00000000); // no interlock or warnings enabled
    self.write(amac_id, 46, 0x00010100)# writeReg(tx, 46  ,  0x00010100); // HCC resets set (takes HCC out of reset)
    self.write(amac_id, 47, 0x00010100) # writeReg(tx, 47  ,  0x00010100); // HCC resets set (takes HCC out of reset)
    self.write(amac_id, 48, 1) # writeReg(tx, 48  ,  0x00000001); // AM enabled 
    self.write(amac_id, 49, 1) # writeReg(tx, 49  ,  0x00000001); // AM enabled
    self.write(amac_id, 50, 0x00000100) # writeReg(tx, 50  ,  0x00000100); // require DCDC PGOOD
    self.write(amac_id, 51, 0x00000100) # writeReg(tx, 51  ,  0x00000100); // require DCDC PGOOD
    self.write(amac_id, 52, bg) # writeReg(tx, 52  ,  0x00008e8c); // PB5 
    self.write(amac_id, 53, 0) # writeReg(tx, 53  ,  0x00000000); // default
    self.write(amac_id, 54, 0) # writeReg(tx, 54  ,  0x00000000); // shunt values at zero
    self.write(amac_id, 55, 0x00000019) # writeReg(tx, 55  ,  0x00000019); // DAC bias at 25 (gives full 1V range on AMAC AM measurement
    self.write(amac_id, 56, 0x00000704) # writeReg(tx, 56  ,  0x00000404); // default or 0x704 ? (drivr current)
    self.write(amac_id, 57, 0x000C0C0C) # writeReg(tx, 57  ,  0x000C0C0C); // default
    self.write(amac_id, 58, 0x00000101) # writeReg(tx, 58  ,  0x00000101); // taking Karol's DCDCoOffset from database

  # Receive the data from the chip without sending anything
  # AMAC does not send data spontaneously
  # def listen(self):
  #   self.report("Capture data from AMAC", verbosity=2)
  #   self.clear_report()
  #   self.listen_and_send_command(None)


  # read a given register from FELIX register map
  def felix_config_get(self, reg_name):
    self.report("Read FELIX register {}".format(reg_name), verbosity=2)
    self.issue_flx_config("get", reg_name)
    if self.process_result.returncode != 0:
      raise ValueError(self.process_result.stderr)
    self.report("{}".format(self.process_result.stdout), verbosity=2)
    out_split = self.process_result.stdout.split("=")
    return out_split[1]
    # inputs e.g. "TIMEOUT_CTRL_ENABLE=0x1"
    # returns stuff to the right of '='

  def parse_response(self, filename):
    data = BitArray(filename=filename)
    if len(data) <= 256:
      self.report("AMAC response: '{}'".format(data.hex), verbosity=2)
    else:
      self.report("AMAC response (truncated, showing first 32 bytes): '{}'".format(data[0:256].hex), verbosity=2)

    # if mode == "write":
    #   if len(data) < 8:
    #     raise Exception("Response from AMAC is too short to parse (was {} bits, expected 8 bits)".format(len(data)))
    #   self.response = {"comm_id" : data[0:5], "seq_id" : data[5:8], "register_data" : None}
    # else:
    #   if len(data) < 40:
    #     raise Exception("Response from AMAC is too short to parse (was {} bits, expected 40 bits)".format(len(data)))
    #   self.response = {"comm_id" : data[0:5], "seq_id" : data[5:8], "register_data" : data[8:40]}

    self.report(self.response, verbosity=2)


  def to_integer(self, arg):
    if isinstance(arg, int):
      return arg
    return int(arg, 0)


  def to_float(self, arg):
    if isinstance(arg, float):
      return arg
    return float(arg)


  def get_crc(self, command):
    crc = BitArray(uint=0, length=8)
    for i in range(8):  
      bits = [x for x in command.cut(1)][i::8] 
      crc[i] = reduce(xor, map(bool, bits))
    return crc    
