#!/usr/bin/env python3


from encdec8b10b import EncDec8B10B
# This library 10b encoding is wrong

from bitstring import BitArray, BitStream
import argparse

__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


# data is a python list of int
def encode_sequence(data, idles, comma, sop, eop, parity):
	result = []
	parity, word_sop = EncDec8B10B.enc_8b10b(sop, parity, 1)	
	result.append(word_sop)
	if args.verbose > 1:
		print(f"parity={parity}")

	for word_in in data:
		parity, word_out = EncDec8B10B.enc_8b10b(word_in, parity)
		result.append(word_out)
		if args.verbose > 1:
			print(f"parity={parity}")

	parity, word_eop = EncDec8B10B.enc_8b10b(eop, parity, 1)
	if args.verbose > 1:
		print(f"parity={parity}")
	result.append(word_eop)

	for i in range(idles):
		parity, word_idle = EncDec8B10B.enc_8b10b(comma, parity)
		result.append(word_idle)
		if args.verbose > 1:
			print(f"parity={parity}")

	return parity, result


# sequence_10b is a python list of int
def reshape_sequence(sequence_10b):	
	bitarrays = [BitArray(uint=word, length=10) for word in sequence_10b]	
	result = BitArray().join(bitarrays)
	if args.verbose:
		print("10b words:")
		for bitarray in bitarrays:
			x = BitArray("0b00") + bitarray
			print(f"{x.hex}", end=",")
		print("")
		print("Joined into bitstring:")
		print(result)
	if result.len % 8:
		raise ValueError(f"Unexpected number of bits={result.len}")
	return result


# data is a BitArray
def print_result(data):
	for byte in data.cut(8):
		print(f"0x{byte.uint:02X}", end=",")
	print("")


def main():
	print("WARNING - the 8b10b encoder used by the script is bugged and "
		+ "is producing erroneous data")

	parser = argparse.ArgumentParser(
		description="Utility for making 8b10b encoded byte sequences")
	parser.add_argument('-d', "--data", type=str,
		help='Packet data (comma separated bytes)', default="e0,f5,78,50,02,30")
	parser.add_argument('-i', '--idles', default=0, type=int,
		help="How many idles to incode between the packets")
	parser.add_argument('-c', '--comma', help="Comma character",
		default="0xBC", type=str)
	parser.add_argument('-s', '--sop', help="Start-Of-Packet control character",
		default="0x3C", type=str)
	parser.add_argument('-e', '--eop', help="End-Of-Packet control character",
		default="0xDC", type=str)
	parser.add_argument('-v', '--verbose', help='Verbose output',
		action="count", default=0)
	parser.add_argument('-p', '--parity', help="Starting parity", default=0, type=int)


	global args
	args = parser.parse_args()
	data = [int(x, 16) for x in args.data.split(',')]
	sop = int(args.sop, 0)
	eop = int(args.eop, 0)
	comma = int(args.comma, 0)
	idles = args.idles
	parity = args.parity

	parity, encoded_data_1 = encode_sequence(data=data, idles=idles,
		comma=comma, sop=sop, eop=eop, parity=parity)

	if parity == args.parity:
		data = reshape_sequence(encoded_data_1)
	else:
		parity, encoded_data_2 = encode_sequence(data=data, idles=idles,
			comma=comma, sop=sop, eop=eop, parity=parity)		
		if parity != args.parity:
			raise ValueError(f"Unexpected parity={parity}")
			data = reshape_sequence(encoded_data_1 + encoded_data_2)

	data = reshape_sequence([0x0f9, 0x18e, 0x2a1, 0x333, 0x245, 0x2d4, 0x1b9,
		0x309, 0x0fa, 0x309, 0x0fa, 0x309, 0x0fa, 0x305, 0x0fa, 0x305])

	print_result(data)



if __name__ == '__main__':
	main()