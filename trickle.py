#!/usr/bin/env python3

import argparse
import os
import csv
import json

from tqdm import tqdm
from termcolor import colored, cprint
from tabulate import tabulate
from bitstring import BitArray, BitStream, ReadError

from trickle.compiler import Compiler
from hcc_star.driver import get_driver
from hcc_star.parser import Parser as HCCParser
from hcc_star.parser import TYP, ParseError


__author__ = "Elena Zhivun"
__version__ = "0.0.1"
__maintainer__ = "Elena Zhivun"
__email__ = "ezhivun@bnl.gov"


lpgbt_map_phase1 = {
  0 : {
	"output_elinks" : "018,01a,01c,01e",
	"input_elinks" : "001,005,009,00d,011,015,019,01d,041,045,049,04d,051,055,059,05d"
  },
  1 : {
	"output_elinks" : "080,086,08a,08e",
	"input_elinks" : "081,085,089,08d,091,095,099,09d,0c1,0c5,0c9,0cd,0d1,0d5,0d9,0dd"
  }
}

lpgbt_map_phase2 = {
  0 : {
	"output_elinks" : "001,006,00b,010",
	"input_elinks" : "000,002,004,006,008,00a,00c,00e,010,012,014,016,018,01a"
  },
  1 : {
	#"output_elinks" : "041,046,04b,050",
	# LCB and R3L1 elinks are swapped on slave side
	"output_elinks" : "041,049,04e,053",
	"input_elinks" : "040,042,044,046,048,04a,04c,04e,050,052,054,056,058,05a"
  }
}


def main():
	parser = argparse.ArgumentParser(description="Utility for testing trickle configuration memory")
	parser.add_argument('command', choices=(["compile", "upload", "start", "stop", "listen", "enable_abc"]), help=('Command to execute'), type=str)
	parser.add_argument('-v', '--verbose', help='Verbose output (-vv = even more verbose)', action="count", default=0)
	parser.add_argument('-src', '--source', help='Path to trickle counfiguration data', required=False, type=str)
	parser.add_argument('-out', '--output', help='Path to directory to save reports', default="./data/reports", required=False, type=str)
	parser.add_argument('-dr', '--dry_run', help='Dry run: no actual commands or data are sent', action="store_true")
	parser.add_argument('-de', '--descriptor', help='FELIX ToHost descriptor number', default=0, required=False, type=int)
	parser.add_argument('-d', '--felix_device', help='FELIX device ID (default 0)', default="0", required=False, type=str)
	parser.add_argument('-p', '--path_to_ftools', help='Specify path to the ftools (fupload, fdaq, fcheck)', default="", required=False, type=str)
	parser.add_argument('-R', '--reset', help='Pass "-R" flag when calling fdaq/fdaqm', action="store_true")
	parser.add_argument('-l', '--lpgbt', help='lpGBT link ID (default 0)', default="0", required=False, type=str)
	parser.add_argument('-drv', '--driver', help='ITk Strips driver version (software, firmware), default software', default="software", required=False, type=str)

	global args
	args = parser.parse_args()

	if args.command != "compile":
		if args.command == "enable_abc":
			driver = get_driver(args.driver)(dry_run=args.dry_run,
				path_to_ftools=args.path_to_ftools,
				felix_device=args.felix_device,
				verbose=(args.verbose),
				nibble_order_to_host="direct",
				nibble_order_from_host="direct",
				raise_on_error=False,
				reset=args.reset,
				descriptor=args.descriptor)

		else:
			driver = get_driver("firmware")(
				dry_run=args.dry_run, felix_device=args.felix_device,
				path_to_ftools=args.path_to_ftools, verbose=args.verbose,			
				nibble_order_to_host="direct", nibble_order_from_host="direct",
				raise_on_error=False, reset=args.reset, fdaq_duration=1,
				ignore_physics_packets=True, descriptor=args.descriptor)

	if args.command == "compile" or args.command == "upload" or args.command == "start" or args.command == "stop":
		if not args.source:
			parser.error('--source path is required for command {}'.format(args.command))
		compiler = Compiler(args.source)
	
	if args.command == "compile":
		compiler.compile()
		print("Successfully prepared trickle configuration contents in {}".format(args.source))

	elif args.command == "upload":
		compiler.compile()
		print("Loading trickle configuration memory from {}".format(args.source))
		for output_data in compiler.outputs:
			print("{} -> segment {} ({} bytes total)".format(output_data["file"],
				output_data["segment"], output_data["length"]))
			driver.load_trickle_memory_from_file(
				os.path.join(args.source, output_data["file"]),
				output_data["segment"],
				output_data["length"])		

	elif args.command == "start":
		for output_data in compiler.outputs:
			driver.trickle_trigger_run(True, output_data["segment"])

	elif args.command == "stop":
		for output_data in compiler.outputs:
			driver.trickle_trigger_run(False, output_data["segment"])

	elif args.command == "enable_abc":
		resolve_elinks(driver)
		enable_abc(driver)

	elif args.command == "listen":
		resolve_elinks(driver)
		listen_and_print_resuts(driver)

	else:
		parser.error("Unsupported command: {}".format(args.command))


# parse all HCC*/ABC*
def parse_response(driver):
	if os.stat(driver.data_filename).st_size == 0:
		return 

	data = BitArray(filename=driver.data_filename)
	parser = HCCParser(data, verbose=False, faster=True)	
	parse_errors = 0

	pbar = tqdm(total=parser.length, unit="bit")
	EOF = 0
	while not EOF:
		try:			
			packet = parser.read_next()
			count_packet(packet, driver.elink_in)
			pbar.update(packet['file_pos_bit'] - pbar.n)
		except ParseError as e:
			parse_errors += 1
		except ReadError as e:
			# print("EOF")
			EOF = 1
			break

	pbar.close()

	if parse_errors != 0:
		print("Encountered {} parse errors".format(parse_errors))


# find which elink IDs to use with this firmware version
def resolve_elinks(driver):
	global input_elinks, output_elinks
	if int(args.lpgbt, 0) in lpgbt_map_phase2:
		regmap_version = driver.regmap_major
		if regmap_version >= 5:
			# this is regmap version >= rm-5.0
			input_elinks_str = lpgbt_map_phase2[int(args.lpgbt, 0)]["input_elinks"]
			output_elinks_str = lpgbt_map_phase2[int(args.lpgbt, 0)]["output_elinks"]
		elif regmap_version >= 4:
			# this is regmap version >= rm-4.0
			input_elinks_str = lpgbt_map_phase1[int(args.lpgbt, 0)]["input_elinks"]
			output_elinks_str = lpgbt_map_phase1[int(args.lpgbt, 0)]["output_elinks"]
		else:
			print("Unsupported REG_MAP_VERSION={}".format(regmap_version))
			return 38
	else:
		print("lpGBT {} support is not implemented")
		# TODO: take elinks from lpGBT_map_phase2 and add (0x40 * lpGBT_ID)
		raise NotImplementedError

	input_elinks = input_elinks_str.split(',')
	output_elinks = output_elinks_str.split(',')

# add packet to a dictionary
def count_packet_helper(packet, dst):
	reg_id = packet["reg_addr"].hex
	reg_data = packet["reg_data"].hex

	if reg_id in dst:
		old_data = dst[reg_id]["data"]
		dst[reg_id]["count"] += 1;
		dst[reg_id]["consistent"] = (
			dst[reg_id]["consistent"] and reg_data == old_data);
	else:
		dst[reg_id] = {			
			"count" : 1,
			"data"  : reg_data,
			"consistent" : True
		}

# count ABC* and HCC* read packets
def count_packet(packet, rx_elink):
	if not (packet["typ"] == TYP.HCC_reg_read or packet["typ"] == TYP.ABC_reg_read):
		return	

	dst = register_reads[packet["typ"]]		
	registers[packet["typ"]].add(packet["reg_addr"].hex)

	if not (rx_elink in dst):
		dst[rx_elink] = {}


	if packet["typ"] == TYP.HCC_reg_read:
		count_packet_helper(packet, dst[rx_elink])

	if packet["typ"] == TYP.ABC_reg_read:
		channel = packet["hcc_channel"]

		if channel in dst[rx_elink]:
			count_packet_helper(packet, dst[rx_elink][channel])

		else:
			reg_id = packet["reg_addr"].hex
			reg_data = packet["reg_data"].hex

			dst[rx_elink][channel] = {
				reg_id : {
					"count" : 1,
					"data"  : reg_data,
					"consistent" : True
				}
			}


# color the register reading statistics
# red = the readout results were inconsistent
# i.e different values were read out from the same register
def reg_read_results(channel, reg_id, reg_read_data):
	if channel in reg_read_data:
		data = reg_read_data[channel]
		if reg_id in data:
			msg = "{} x{}".format(data[reg_id]["data"], data[reg_id]["count"])
			if data[reg_id]["consistent"]:
				return msg
			else:
				return colored(msg, "red")
		else:
			return colored("N/A", "red")
	else:
		return colored("N/A", "red") # data from that chip* is missing


def build_table(header, reg_ids, channels, reg_read_data):
	table = [header]
	global register_count;
	register_count = 0;
	for reg_id in reg_ids:
		reg_id_int = int(reg_id, 16)
		line = ["{} (0x{:x})".format(reg_id_int, reg_id_int)]
		for channel in channels:
			if channel in reg_read_data:
				data = reg_read_data[channel]
				if reg_id in data:
					register_count += 1
					msg = "{}".format(data[reg_id]["data"])
					if data[reg_id]["consistent"]:
						line.append(msg)
					else:
					 	line.append(colored(msg, "red"))
				else:
					line.append("N/A")
			else:
				line.append("N/A")
		table.append(line)
	return table


# save the report as a csv file
def export_csv_report(table, file_name):
	report_path = os.path.join(args.output, file_name)
	with open(report_path, 'w', newline='') as csvfile:
		report_writer = csv.writer(csvfile)
		for line in table:
			report_writer.writerow(line)


# print all HCC* register reads as a table
def print_hcc_packets():
	print("")
	if not register_reads[TYP.HCC_reg_read]:
		print("No HCC* read packets were received")
		return
	
	reg_ids = sorted(registers[TYP.HCC_reg_read])
	rx_elinks = sorted(register_reads[TYP.HCC_reg_read].keys())
	header = ["Reg. ID \\ RX elink"]
	header.extend(rx_elinks)	
	table = build_table(header, reg_ids, rx_elinks, register_reads[TYP.HCC_reg_read])

	print(tabulate(table, headers="firstrow"))
	print("Total {} registers".format(register_count))
	export_csv_report(table, "hcc_trickle_results.csv")
	

# print table with results of ABC* register readout
def print_abc_packets(rx_elink, abc_reg_reads):	
	reg_ids = sorted(registers[TYP.ABC_reg_read])
	abc_ids = sorted(abc_reg_reads.keys())
	header = ["Reg. ID \\ ABC* ID"]
	header.extend(abc_ids)
	table = build_table(header, reg_ids, abc_ids, abc_reg_reads)

	print(tabulate(table, headers="firstrow"))
	print("Total {} registers".format(register_count))
	export_csv_report(table, "rx_{}_abc_trickle_results.csv".format(rx_elink))


# print all ABC* register reads as a set of tables
def print_abc_packets_all():
	if not register_reads[TYP.ABC_reg_read]:
		print("No ABC* read packets were received")
		return

	for rx_elink, abc_reg_reads in register_reads[TYP.ABC_reg_read].items():
		print("")
		print("ABC* connected to RX elink {}".format(rx_elink))
		print_abc_packets(rx_elink, abc_reg_reads)


# acquire some data with fdaq and print a table of all "register read" responses received
def listen_and_print_resuts(driver):	
	driver.do_parse_response = False
	print("Capturing FELIX data sample (device #{} descriptor #{})".format(args.felix_device, args.descriptor)) 
	driver.listen()

	global register_reads #, hcc_mapping
	global registers
	register_reads = {
		TYP.HCC_reg_read : {},
		TYP.ABC_reg_read : {}
	}

	registers = {
		TYP.HCC_reg_read : set(),
		TYP.ABC_reg_read : set()
	}

	if not os.path.exists(args.output):
		os.makedirs(args.output)

	for input_elink in input_elinks:
		print("Counting register_read responses on ToHost elink {}".format(input_elink))
		driver.elink_in = input_elink
		driver.issue_fcheck()
		parse_response(driver)

	with open(os.path.join(args.output, "trickle_stats.json"), 'w') as f:
		json.dump(register_reads, f, indent=4)

	print_hcc_packets()
	print_abc_packets_all()


def enable_abc(driver):
	driver.target = "HCC"
	driver.hcc_id = "0b1111"
	driver.abc_id = "0b1111"

	for output_elink in output_elinks:
		print("Configuring ABC* on FromHost elink {}".format(output_elink))      
		driver.elink_out = output_elink
		driver.enable_ABC_star()
		driver.configure_ABC_star()
		#Enable ABC* register read function (write 0x400 to ABC* register 0x20)
		driver.write_register(0x20, 0x400, target="ABC")


if __name__ == '__main__':
  main()
