#!/bin/bash

echo "Enable R3 and L1 trigger for Strips link 0"
flx-config setraw -r 0xD1E0 -o 0 -w 2 -v 3

echo "Set mask field for simulated R3 frame"
flx-config setraw -r 0xD000 -o 11 -w 5 -v 0x15

echo "Set tag field for simulated R3/L1 frame"
flx-config setraw -r 0xD000 -o 4 -w 7 -v 0x2A

sleep 3

# If the trigger doesn't come out, check this offset
# it's unofficial and unstable, and changes from build to build
echo "Send simulated R3 and L1 frames simultaneously"
flx-config setraw -r 0xD840 -o 0 -w 64 -v 1
